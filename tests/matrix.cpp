/*:*
  Copyright Jean-Denis Lesage (2011)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include "matrix.h"
#include "gtest/gtest.h"
#include <boost/math/constants/constants.hpp>

using namespace raytrace;

TEST(Matrix, TranslationPoint)
{
    const Vector4 result = translation(1, 3, -2) * Vector4({4, -2, 1, 1});
    EXPECT_NEAR(5, result[0], g_tolerance);
    EXPECT_NEAR(1, result[1], g_tolerance);
    EXPECT_NEAR(-1, result[2], g_tolerance);
    EXPECT_NEAR(1, result[3], g_tolerance);
}

TEST(Matrix, TranslationVector)
{
    const Vector4 result = translation(1, 3, -2) * Vector4({4, -2, 1, 0});
    EXPECT_NEAR(4, result[0], g_tolerance);
    EXPECT_NEAR(-2, result[1], g_tolerance);
    EXPECT_NEAR(1, result[2], g_tolerance);
    EXPECT_NEAR(0, result[3], g_tolerance);
}

TEST(Matrix, ScalePoint)
{
    const Vector4 result = scale(1, 3, -2) * Vector4({4, -2, 1, 1});
    EXPECT_NEAR(4, result[0], g_tolerance);
    EXPECT_NEAR(-6, result[1], g_tolerance);
    EXPECT_NEAR(-2, result[2], g_tolerance);
    EXPECT_NEAR(1, result[3], g_tolerance);
}

TEST(Matrix, ScaleVector)
{
    const Vector4 result = scale(1, 3, -2) * Vector4({4, -2, 1, 0});
    EXPECT_NEAR(4, result[0], g_tolerance);
    EXPECT_NEAR(-6, result[1], g_tolerance);
    EXPECT_NEAR(-2, result[2], g_tolerance);
    EXPECT_NEAR(0, result[3], g_tolerance);
}

#define COS(x) cosf(x)
#define SIN(x) sinf(x)

TEST(Matrix, RotationX)
{
    const Real_t angle = boost::math::constants::pi<Real_t>()/4.0f;
    Matrix44 rx({1, 0, 0, 0,
            0, COS(angle), SIN(angle), 0,
            0, -SIN(angle), COS(angle), 0,
            0, 0, 0, 1});
    EXPECT_TRUE(equal(rx, rotation(Vector3({1,0,0}), angle)));
}

TEST(Matrix, RotationY)
{
    const Real_t angle = boost::math::constants::pi<Real_t>()/4.0f;
    Matrix44 ry({COS(angle), 0, -SIN(angle), 0,
            0, 1, 0, 0,
            SIN(angle), 0, COS(angle), 0,
            0, 0, 0, 1});
    EXPECT_TRUE(equal(ry, rotation(Vector3({0,1,0}), angle)));
}

TEST(Matrix, RotationZ)
{
    const Real_t angle = boost::math::constants::pi<Real_t>()/4.0f;
    Matrix44 rz({COS(angle), SIN(angle), 0, 0,
            -SIN(angle), COS(angle), 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1});
    EXPECT_TRUE(equal(rz, rotation(Vector3({0,0,2}), angle)));
}

TEST(Matrix, Perspective)
{
    // -1 -> 1
    Matrix44 MatrixPerspec({-1,0,0,0,
            0,-1,0,0,
            0,0,0,1,
            0,0,1,0});
    EXPECT_TRUE(equal(MatrixPerspec, perspective(-1,1,-1,1,-1,1)));
}
