#include <boost/assign.hpp>
#include <boost/chrono.hpp>
#include "matrix.h"
#include "mesh.h"
#include "scene.h"


using namespace boost::chrono;
using namespace raytrace;


const std::vector<Vector3> vertices = boost::assign::list_of
                                        (Vector3({-0.5, -0.5, -0.5}))
                                        (Vector3({ 0.5, -0.5, -0.5}))
                                        (Vector3({ 0.5,  0.5, -0.5}))
                                        (Vector3({-0.5,  0.5, -0.5}))
                                        (Vector3({-0.5, -0.5,  0.5}))
                                        (Vector3({ 0.5, -0.5,  0.5}))
                                        (Vector3({ 0.5,  0.5,  0.5}))
                                        (Vector3({-0.5,  0.5,  0.5}));

const std::vector<size_t> indices = boost::assign::list_of
                                        (0)(2)(1)
                                        (0)(3)(2)
                                        (1)(6)(5)
                                        (1)(2)(6)
                                        (3)(6)(2)
                                        (3)(7)(6)
                                        (0)(7)(3)
                                        (0)(4)(7)
                                        (4)(6)(7)
                                        (4)(5)(6)
                                        (0)(5)(4)
                                        (0)(1)(5);

typedef duration<int64_t, boost::milli> milliseconds;

const uint32_t NB_ITERATIONS = 20;

int main(int, char**)
{
    Scene scene;
    Mesh* pMesh = new Mesh;
    pMesh->setVertexBuffer(vertices);
    pMesh->setIndexBuffer(indices);
    Matrix44 transformation = rotation({0, 1, 0}, 3.14/4.0) * rotation({1, 0, 0}, 3.14/4.0);
    pMesh->setTransformation(transformation);
    std::shared_ptr<Object> pObject(pMesh);
    scene.addObject(pObject);
    high_resolution_clock::time_point dStart = high_resolution_clock::now();
    for(uint32_t i = 0; i != NB_ITERATIONS; ++i)
        scene.render();
    high_resolution_clock::time_point dEnd = high_resolution_clock::now();
    const milliseconds us = duration_cast<milliseconds>(dEnd - dStart);
    std::cout << "FPS = " << static_cast<double>(NB_ITERATIONS) / (static_cast<double>(us.count()) * 0.001) << std::endl;
    return 0;
}
