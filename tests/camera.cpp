/**
  Copyright Jean-Denis Lesage (2011)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include "camera.h"
#include "gtest/gtest.h"

using namespace raytrace;

TEST(Camera, CameraInitialization)
{
    Camera c;
    EXPECT_DOUBLE_EQ(0, c.getPosition()[0]);
    EXPECT_DOUBLE_EQ(0, c.getPosition()[1]);
    EXPECT_DOUBLE_EQ(0, c.getPosition()[2]);

    EXPECT_DOUBLE_EQ(0, c.getTarget()[0]);
    EXPECT_DOUBLE_EQ(0, c.getTarget()[1]);
    EXPECT_DOUBLE_EQ(1, c.getTarget()[2]);

    EXPECT_DOUBLE_EQ(0, c.getUp()[0]);
    EXPECT_DOUBLE_EQ(1, c.getUp()[1]);
    EXPECT_DOUBLE_EQ(0, c.getUp()[2]);
}

TEST(Camera, invModelView)
{
    Camera c;
    c.setPosition(Vector3({1,0,-1}));
    Vector3 Y({0,0,1}); 
    Vector3 Z({1,1,0});
    Vector3 X({-1,1,0});
    c.setTarget(Z);
    c.setUp(Y);
    Z /= Z.norm();
    X /= X.norm();
    Matrix44 tmp({X[0], Y[0], Z[0], 1,
            X[1], Y[1], Z[1], 0,
            X[2], Y[2], Z[2], -1,
            0, 0, 0, 1});
    Matrix44 identity;
    EXPECT_TRUE(equal(tmp, c.invViewMatrix())); 
    EXPECT_TRUE(equal(identity, tmp.inv()*c.invViewMatrix())); 
}

TEST(Camera, RayMiddle)
{
    Camera c;
    c.setPosition(Vector3({1,0,-1}));
    c.setViewPort(3, 3);
    Ray r = c.getRayFromPixel(1, 1);
    EXPECT_TRUE(equal(c.getPosition(), r.getSource()));
    EXPECT_TRUE(equal(Vector3({0, 0, 1}), r.getDirection()));
}

TEST(Camera, RayLowerCorner)
{
    Camera c;
    c.setPosition(Vector3({1,0,-1}));
    c.setViewPort(3, 3);
    Ray r = c.getRayFromPixel(0, 2);
    Vector3 expectedDirection({-1,-1,1});
    expectedDirection /= expectedDirection.norm();
    EXPECT_TRUE(equal(c.getPosition(), r.getSource()));
    EXPECT_TRUE(equal(expectedDirection, r.getDirection()));
}

TEST(Camera, RayUpperCorner)
{
    Camera c;
    c.setPosition(Vector3({1,0,-1}));
    c.setViewPort(3, 3);
    Ray r = c.getRayFromPixel(2, 0);
    Vector3 expectedDirection({1,1,1});
    expectedDirection /= expectedDirection.norm();
    EXPECT_TRUE(equal(c.getPosition(), r.getSource()));
    EXPECT_TRUE(equal(expectedDirection, r.getDirection()));
}

TEST(Camera, RayLowerCornerFourThird)
{
    Camera c;
    c.setPosition(Vector3({1,0,-1}));
    c.setFrustrum(-4.0/3.0, 4.0/3.0, -1, 1, 1, 2);
    c.setViewPort(800, 600);
    Ray r = c.getRayFromPixel(0, 599);
    Vector3 expectedDirection({-4.0/3.0,-1,1});
    expectedDirection /= expectedDirection.norm();
    EXPECT_TRUE(equal(c.getPosition(), r.getSource()));
    EXPECT_TRUE(equal(expectedDirection, r.getDirection()));
}


