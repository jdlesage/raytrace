/**
  Copyright Jean-Denis Lesage (2011)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include "matrix.h"
#include "sphere.h"
#include "gtest/gtest.h"
#include <boost/math/constants/constants.hpp>

using namespace raytrace;

TEST(Sphere, IntersectRadiusEqual2)
{
    Sphere s;
    s.setRadius(2.0);
    Ray r1(Vector4({1.9, 0, -10, 1}), Vector4({0, 0, 1, 0}));
    EXPECT_TRUE(s.intersect(r1).impact);

    Ray r2(Vector4({2.1, 0, -10, 1}), Vector4({0, 0, 1, 0}));
    EXPECT_FALSE(s.intersect(r2).impact);
}

TEST(Sphere, IntersectCenter2_0_0)
{
    Sphere s;
    s.setCenter(Vector3({2, 0, 0}));
    Ray r1(Vector4({1.1, 0, -10, 1}), Vector4({0, 0, 1, 0}));
    EXPECT_TRUE(s.intersect(r1).impact);

    Ray r2(Vector4({0.9, 0, -10, 1}), Vector4({0, 0, 1, 0}));
    EXPECT_FALSE(s.intersect(r2).impact);
}

TEST(Sphere, BehindCameraDoesNotIntersect)
{
    Sphere s;
    s.setCenter(Vector3({0, 0, -2}));
    Ray r(Vector4({0, 0, 0, 1}), Vector4({0, 0, 1, 0}));
    EXPECT_FALSE(s.intersect(r).impact);
}

TEST(Sphere, DistanceMin)
{
    Sphere s;
    s.setRadius(4.2);
    Ray r(Vector4({0, 0, -15.576, 1}), Vector4({0, 0, 1, 0}));
    EXPECT_NEAR(11.376, s.intersect(r).distance, g_tolerance);
}

TEST(Sphere, DistanceL2Only)
{
    Sphere s;
    s.setRadius(3.6);
    s.setCenter(Vector3({0, 0, 0.4}));
    Ray r(Vector4({0, 0, 0, 1}), Vector4({0, 0, 1, 0}));
    EXPECT_NEAR(4, s.intersect(r).distance, g_tolerance);
}

TEST(Sphere, DistanceL1Only)
{
    Sphere s;
    s.setRadius(3.6);
    s.setCenter(Vector3({0, 0, 0.4}));
    Ray r(Vector4({0, 0, 0, 1}), Vector4({0, 0, -1, 0}));
    EXPECT_NEAR(3.2, s.intersect(r).distance, g_tolerance);
}

TEST(Sphere, DistanceAndScale)
{
    Sphere s;
    s.setTransformation(scale(3, 3, 3));
    Ray r(Vector4({0, 0, 0, 1}), Vector4({0, 0, -1, 0}));
    EXPECT_DOUBLE_EQ(3, s.intersect(r).distance);
}

TEST(Sphere, ComputeNormal1)
{
    Sphere s;
    Ray r(Vector4({0, 0, -4, 1}), Vector4({0, 0, 1, 0}));
    const Vector3 normal = s.intersect(r).normal;
    EXPECT_TRUE(equal(Vector3({0, 0, -1}), normal));
}

TEST(Sphere, ComputeNormal2)
{
    Sphere s;
    Ray r(Vector4({0, 0, 0, 1}), Vector4({0, 0, 1, 0}));
    const Vector3 normal = s.intersect(r).normal;
    EXPECT_TRUE(equal(Vector3({0, 0, 1}), normal));
}

TEST(Sphere, ComputeNormal3)
{
    Sphere s;
    const Real_t invSqrt3 = 1.0/sqrt(3.0);
    Ray r(Vector4({invSqrt3, invSqrt3, 0, 1}), Vector4({0, 0, 1, 0}));
    const Vector3 normal = s.intersect(r).normal;
    EXPECT_TRUE(equal(Vector3({invSqrt3, invSqrt3, invSqrt3}), normal));
}

TEST(Sphere, ComputeNormalScale)
{
    Sphere s;
    s.setTransformation(scale(3, 3, 3));
    Ray r(Vector4({0, 0, -4, 1}), Vector4({0, 0, 1, 0}));
    const Vector3 normal = s.intersect(r).normal;
    EXPECT_TRUE(equal(Vector3({0, 0, -1}), normal));
}

TEST(Sphere, ComputeNormalRadius)
{
    Sphere s;
    s.setRadius(1.0/3.0);
    Ray r(Vector4({0, 0, -4, 1}), Vector4({0, 0, 1, 0}));
    const Vector3 normal = s.intersect(r).normal;
    EXPECT_TRUE(equal(Vector3({0, 0, -1}), normal));
}

TEST(Sphere, ComputeNormalRadiusAndScale)
{
    Sphere s;
    s.setRadius(1.0/3.0);
    s.setTransformation(scale(3, 3, 3));
    Ray r(Vector4({0, 0, -4, 1}), Vector4({0, 0, 1, 0}));
    const Vector3 normal = s.intersect(r).normal;
    EXPECT_TRUE(equal(Vector3({0, 0, -1}), normal));
}



TEST(Sphere, ComputeNormalCenterMove)
{
    Sphere s;
    const Real_t invSqrt3 = 1.0/sqrt(3.0);
    s.setCenter(Vector3({-invSqrt3, -invSqrt3, 0}));
    Ray r(Vector4({0, 0, 0, 1}), Vector4({0, 0, 1, 0}));
    const Vector3 normal = s.intersect(r).normal;
    EXPECT_TRUE(equal(Vector3({invSqrt3, invSqrt3, invSqrt3}), normal));
}

TEST(Sphere, ComputeNormalRotation)
{
    Sphere s;
    s.setTransformation(rotation(Vector3({0, 1, 0}), boost::math::constants::pi<Real_t>()/2.0));
    Ray r(Vector4({0, 0, -4, 1}), Vector4({0, 0, 1, 0}));
    const Vector3 normal = s.intersect(r).normal;
    EXPECT_TRUE(equal(Vector3({0, 0, -1}), normal));
}


