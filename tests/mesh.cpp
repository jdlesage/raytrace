/**
  Copyright Jean-Denis Lesage (2011)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include "compareImage.h"
#include "matrix.h"
#include "mesh.h"
#include "scene.h"
#include "gtest/gtest.h"

using namespace raytrace;


const std::vector<Vector3> vertices({
                                    Vector3({-0.5, -0.5, -0.5}),
                                    Vector3({ 0.5, -0.5, -0.5}),
                                    Vector3({ 0.5,  0.5, -0.5}),
                                    Vector3({-0.5,  0.5, -0.5}),
                                    Vector3({-0.5, -0.5,  0.5}),
                                    Vector3({ 0.5, -0.5,  0.5}),
                                    Vector3({ 0.5,  0.5,  0.5}),
                                    Vector3({-0.5,  0.5,  0.5})});


const std::vector<size_t> indices({0, 2, 1,
                                   0, 3, 2,
                                   1, 6, 5,
                                   1, 2, 6,
                                   3, 6, 2,
                                   3, 7, 6,
                                   0, 7, 3,
                                   0, 4, 7,
                                   4, 6, 7,
                                   4, 5, 6,
                                   0, 5, 4,
                                   0, 1, 5});

class MeshTestable: public Mesh
{
    public:
        MeshTestable() : Mesh() {}

        bool triangleBufferIsValid() const {return m_triangleBufferIsValid;}
        size_t nbTriangles() const {return m_triangles->size();}
};

TEST(Mesh, MeshEmptyIsValid)
{
    MeshTestable m;
    EXPECT_TRUE(m.triangleBufferIsValid());
    EXPECT_EQ((size_t) 0, m.nbTriangles());
}

TEST(Mesh, MeshOnlyVertexIsValid)
{
    MeshTestable m;
    m.setVertexBuffer(vertices);
    EXPECT_TRUE(m.triangleBufferIsValid());
    EXPECT_EQ((size_t) 0, m.nbTriangles());
}

TEST(Mesh, MeshOnlyIndexIsNotValid)
{
    MeshTestable m;
    m.setIndexBuffer(indices);
    EXPECT_FALSE(m.triangleBufferIsValid());
}

TEST(Mesh, MeshCubeIsValid)
{
    MeshTestable m;
    m.setVertexBuffer(vertices);
    m.setIndexBuffer(indices);
    EXPECT_TRUE(m.triangleBufferIsValid());
    EXPECT_EQ((size_t) 12, m.nbTriangles());
}

TEST(Mesh, MeshNotTriangleIsNotValid)
{
    MeshTestable m;
    m.setVertexBuffer(vertices);
    const std::vector<size_t> index({0, 1, 2, 3});
    m.setIndexBuffer(index);
    EXPECT_FALSE(m.triangleBufferIsValid());
}

TEST(Mesh, MeshIndexOutsideRangeIsNotValid)
{
    MeshTestable m;
    m.setVertexBuffer(vertices);
    const std::vector<size_t> index({0, 1, 42});
    m.setIndexBuffer(index);
    EXPECT_FALSE(m.triangleBufferIsValid());
}

TEST(Mesh, RenderCube)
{
    Scene scene;
    Mesh* pMesh = new Mesh;
    pMesh->setVertexBuffer(vertices);
    pMesh->setIndexBuffer(indices);
    Matrix44 transformation = rotation({0, 1, 0}, 3.14/4.0) * rotation({1, 0, 0}, 3.14/4.0);
    pMesh->setTransformation(transformation);
    std::shared_ptr<Object> pObject(pMesh);
    scene.addObject(pObject);
    QImage result = scene.render();
    EXPECT_TRUE(compareImage(result, "cube.png"));
}
