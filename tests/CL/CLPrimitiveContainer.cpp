/**
  Copyright Jean-Denis Lesage (2011)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include <boost/cast.hpp>
#include <boost/math/constants/constants.hpp>
#include <memory>
#include "gtest/gtest.h"
#include "camera.h"
#include "matrix.h"
#include "mesh.h"
#include "triangleFactory.h"
#include "CL/CLPrimitiveContainer.h"

using namespace raytrace;
using namespace raytrace::CL;

const std::vector<Vector3> vertices({Vector3({-1, -1, 0}),
                                     Vector3({1, -1, 0}),
                                     Vector3({0, 1, 0}),
                                     Vector3({-1, -1, -1}),
                                     Vector3({1, -1, -1}),
                                     Vector3({0, 1, -1})});

const std::vector<Vector3> verticesCube({ 
                                    Vector3({-0.5, -0.5, -0.5}),
                                    Vector3({ 0.5, -0.5, -0.5}),
                                    Vector3({ 0.5,  0.5, -0.5}),
                                    Vector3({-0.5,  0.5, -0.5}),
                                    Vector3({-0.5, -0.5,  0.5}),
                                    Vector3({ 0.5, -0.5,  0.5}),
                                    Vector3({ 0.5,  0.5,  0.5}),
                                    Vector3({-0.5,  0.5,  0.5})});

const std::vector<size_t> indices({0, 2, 1,
                                   0, 3, 2,
                                   1, 6, 5,
                                   1, 2, 6,
                                   3, 6, 2,
                                   3, 7, 6,
                                   0, 7, 3,
                                   0, 4, 7,
                                   4, 6, 7,
                                   4, 5, 6,
                                   0, 5, 4,
                                   0, 1, 5});

namespace 
{
    Impact findFirstImpact(const CLPrimitiveContainer& container, const Ray& ray)
    {
        std::vector<Ray> rays;
        rays.push_back(ray);
        std::vector<Impact> impacts;
        impacts.resize(1);
        container.findFirstImpact(rays, impacts);
        return impacts[0];
    }

}

TEST(CLPrimitiveContainer, IntersectFirstTriangleEmpty)
{
    CLPrimitiveContainer container;
    container.lock();
    Ray r(Vector4({0, 0, 1, 1}), Vector4({0, 0, -1, 0}));
    EXPECT_FALSE(findFirstImpact(container, r).impact);
}

TEST(CLPrimitiveContainer, IntersectFirstTriangleOneTriangle)
{
    CLPrimitiveContainer container;
    std::shared_ptr<Object> pTriangle(boost::polymorphic_cast<Object*>(createTriangle(vertices, 0, 1, 2)));    
    container.addObject(pTriangle);
    container.lock();
    Ray r(Vector4({0, 0, 1, 1}), Vector4({0, 0, -1, 0}));
    EXPECT_TRUE(findFirstImpact(container, r).impact);
    Ray r2(Vector4({1, 1, 1, 1}), Vector4({0, 0, -1, 0}));
    EXPECT_FALSE(findFirstImpact(container, r2).impact);
}

TEST(CLPrimitiveContainer, IntersectFirstTriangle_TwoTriangles)
{
    CLPrimitiveContainer container;
    std::shared_ptr<Object> pTriangle1(boost::polymorphic_cast<Object*>(createTriangle(vertices, 0, 1, 2)));    
    container.addObject(pTriangle1);
    std::shared_ptr<Object> pTriangle2(boost::polymorphic_cast<Object*>(createTriangle(vertices, 3, 4, 5)));    
    container.addObject(pTriangle2);
    container.lock();
    Ray r(Vector4({0, 0, 1, 1}), Vector4({0, 0, -1, 0}));
    EXPECT_TRUE(findFirstImpact(container, r).impact);
    EXPECT_NEAR(1.0, findFirstImpact(container, r).distance, g_tolerance);
    Ray r2(Vector4({0, 0, -0.5, 1}), Vector4({0, 0, -1, 0}));
    EXPECT_TRUE(findFirstImpact(container, r2).impact);
    EXPECT_NEAR(0.5, findFirstImpact(container, r2).distance, g_tolerance);
    Ray r3(Vector4({0, 0, -1.5, 1}), Vector4({0, 0, -1, 0}));
    EXPECT_FALSE(findFirstImpact(container, r3).impact);
}

TEST(CLPrimitiveContainer, IntersectCube)
{
    CLPrimitiveContainer container;
    std::shared_ptr<Mesh> pMesh(new Mesh);
    pMesh->setVertexBuffer(verticesCube);
    pMesh->setIndexBuffer(indices);
    Matrix44 transformation = rotation({0, 1, 0}, boost::math::constants::pi<double>()/4.0);
    pMesh->setTransformation(transformation);
    std::shared_ptr<Object> pObject = std::static_pointer_cast<Object>(pMesh);
    container.addObject(pObject);
    container.lock();
    const Ray r(Vector4({1, 0, 1, 1}), Vector4({-1.0/sqrt(2.0), 0, -1.0/sqrt(2.0), 0}));
    const Impact i = findFirstImpact(container, r);
    EXPECT_TRUE(i.impact);
    EXPECT_TRUE(equal(i.normal, Vector3({1.0/sqrt(2.0), 0, 1.0/sqrt(2.0)})));
    EXPECT_NEAR(sqrt(2) - 0.5, i.distance, g_tolerance);
}

TEST(CLPrimitiveContainer, IntersectCube2)
{
    CLPrimitiveContainer container;
    std::shared_ptr<Mesh> pMesh(new Mesh);
    pMesh->setVertexBuffer(verticesCube);
    pMesh->setIndexBuffer(indices);
    Matrix44 transformation = rotation({0, 1, 0}, boost::math::constants::pi<double>()/4.0);
    pMesh->setTransformation(transformation);
    std::shared_ptr<Object> pObject = std::static_pointer_cast<Object>(pMesh);
    container.addObject(pObject);
    container.lock();
    const Vector4 expectedImpact({sqrt(2)/4, 0, sqrt(2)/4, 1});
    const Ray r(Vector4({0, 0, 1, 1}), expectedImpact - Vector4({0, 0, 1, 1}));
    const Impact i = findFirstImpact(container, r);
    EXPECT_TRUE(i.impact);
    EXPECT_TRUE(equal(i.normal, Vector3({1.0/sqrt(2.0), 0, 1.0/sqrt(2.0)})));
    EXPECT_NEAR((expectedImpact - Vector4({0, 0, 1, 1})).norm(), i.distance, g_tolerance);
}

TEST(CLPrimitiveContainer, IntersectCube3)
{
    Camera cam;
    cam.setPosition(Vector3({0, 0, -2}));
    cam.setFrustrum(-4.0/3.0, 4.0/3.0 , -1, 1, 1, 2);
    CLPrimitiveContainer container;
    std::shared_ptr<Mesh> pMesh(new Mesh);
    pMesh->setVertexBuffer(verticesCube);
    pMesh->setIndexBuffer(indices);
    Matrix44 transformation = rotation({0, 1, 0}, boost::math::constants::pi<double>()/4.0)* rotation({1, 0, 0}, boost::math::constants::pi<double>()/4.0);
    pMesh->setTransformation(transformation);
    std::shared_ptr<Object> pObject = std::static_pointer_cast<Object>(pMesh);
    container.addObject(pObject);
    container.lock();
    const Ray r = cam.getRayFromPixel(343, 217); 
    const Impact i = findFirstImpact(container, r);
    EXPECT_TRUE(i.impact);
    EXPECT_TRUE(equal(i.normal, Vector3({-1.0/sqrt(2.0), 0, -1.0/sqrt(2.0)})));
    EXPECT_NEAR(1.67978, i.distance, g_tolerance);
}
