/**
  Copyright Jean-Denis Lesage (2011)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include "gtest/gtest.h"
#include "CL/triangleIntersector.h"
#include "triangleFactory.h"

using namespace raytrace;

TEST(CL_TriangleIntersector, Initialization)
{
    EXPECT_NO_THROW(CL::TriangleIntersector intersector);
}

TEST(CL_TriangleIntersector, ComputeIntersectionRays)
{
    const std::vector<Ray> rays({Ray(Vector4({0, 0, 1, 1}), Vector4({0, 0, -1, 0})),
                                 Ray(Vector4({0, 0, -0.5, 1}), Vector4({0, 0, -1, 0})),
                                 Ray(Vector4({0, 0, -1.5, 1}), Vector4({0, 0, -1, 0}))});
    const std::vector<Vector3> vertices({Vector3({-1, -1, 0}),
                                     Vector3({1, -1, 0}),
                                     Vector3({0, 1, 0}),
                                     Vector3({-1, -1, -1}),
                                     Vector3({1, -1, -1}),
                                     Vector3({0, 1, -1})});

    const std::vector<std::shared_ptr<ITriangle> > triangles({
            std::shared_ptr<ITriangle>(createTriangle(vertices, 0, 1, 2)),
            std::shared_ptr<ITriangle>(createTriangle(vertices, 3, 4, 5))});
    std::vector<Impact> impacts;
    CL::TriangleIntersector intersector;
    EXPECT_NO_THROW(intersector.uploadGeometry(triangles));
    EXPECT_NO_THROW(intersector.computeIntersections(rays, impacts));
    ASSERT_EQ((size_t) 3, impacts.size());
    EXPECT_TRUE(impacts[0].impact);
    EXPECT_NEAR(1.0, impacts[0].distance, g_tolerance);
    EXPECT_TRUE(impacts[1].impact);
    EXPECT_NEAR(0.5, impacts[1].distance, g_tolerance);
    EXPECT_FALSE(impacts[2].impact);
}
