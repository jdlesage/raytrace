/**
  Copyright Jean-Denis Lesage (2011)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include "compareImage.h"
#include "matrix.h"
#include "object.h"
#include "scene.h"
#include "sphere.h"
#include "gtest/gtest.h"

using namespace raytrace;

Ray r(Vector4({0,0,1,1}), Vector4({1,0,0,0}));
Matrix44 transformation = rotation(Vector3({1, 0, 0}), 3.0/4.0);  

class ObjectTestable : public Object
{
    public:
        ObjectTestable() : Object() {}
        virtual ~ObjectTestable() {}

    protected:
        virtual Impact intersectInObjectSpace(const Ray& ray) const
        {
            Impact result;
            Ray rayOrig = transformation * ray;
            EXPECT_TRUE(equal(rayOrig.getSource(), r.getSource()));
            EXPECT_TRUE(equal(rayOrig.getDirection(), r.getDirection()));
            result.impact =true;
            // Add a fake normal
            result.normal = Vector3({1, 0, 0});
            return result;
        } 
};


TEST(Object, InvTransformationIntersectRay)
{
    ObjectTestable obj;
    obj.setTransformation(transformation);
    obj.intersect(r);
}

TEST(Object, TranslateObject)
{
    Scene s;
    std::shared_ptr<Object> pSphere(new Sphere);
    pSphere->setTransformation(translation(1, 0, 0));
    s.addObject(pSphere);
    const QImage im = s.render();
    EXPECT_TRUE(compareImage(im, "sphereTranslated.png"));
}

TEST(Object, ScaleObject)
{
    Scene s;
    std::shared_ptr<Object> pSphere(new Sphere);
    pSphere->setTransformation(scale(0.5, 0.5, 0.5));
    s.addObject(pSphere);
    const QImage im = s.render();
    EXPECT_TRUE(compareImage(im, "sphereScaled.png"));
}
