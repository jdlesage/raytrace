/**
  Copyright Jean-Denis Lesage (2011)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include "gtest/gtest.h"
#include <boost/assign.hpp>
#include <memory>
#include "bspTree.h"
#include "triangleFactory.h"
#include "types.h"

using namespace raytrace;


const std::vector<Vector3> vertices = boost::assign::list_of
                                    (Vector3({-0.5, -0.5, -0.5}))
                                    (Vector3({ 0.5, -0.5, -0.5}))
                                    (Vector3({ 0.5,  0.5, -0.5}))
                                    (Vector3({-0.5,  0.5, -0.5}))
                                    (Vector3({-0.5, -0.5,  0.5}))
                                    (Vector3({ 0.5, -0.5,  0.5}))
                                    (Vector3({ 0.5,  0.5,  0.5}))
                                    (Vector3({-0.5,  0.5,  0.5}));

const std::vector<size_t> indices = boost::assign::list_of
                                    (0)(2)(1)
                                    (0)(3)(2)
                                    (1)(6)(5)
                                    (1)(2)(6)
                                    (3)(6)(2)
                                    (3)(7)(6)
                                    (0)(7)(3)
                                    (0)(4)(7)
                                    (4)(6)(7)
                                    (4)(5)(6)
                                    (0)(5)(4)
                                    (0)(1)(5);

TEST(BSPTree, Build)
{
    std::vector<std::shared_ptr<ITriangle> > triangles;
    for(size_t i = 0; i < indices.size(); i+=3)
    {
        triangles.emplace_back(createTriangle(vertices, indices[i], indices[i+1], indices[i+2]));
    }
    std::unique_ptr<BSPTree> pTree(BSPTree::buildBSPTree(triangles));
    EXPECT_TRUE(pTree->isValid());
}

TEST(BSPTree, OneTriangle)
{
    std::vector<std::shared_ptr<ITriangle> > triangles;
    triangles.emplace_back(createTriangle(vertices, 0, 1, 2));
    std::unique_ptr<BSPTree> pTree(BSPTree::buildBSPTree(triangles));
    ViewerBSPTree viewer(*pTree);
    
    const std::vector<std::shared_ptr<ITriangle> >& trianglesOnRoot = viewer.getTriangleOnRoot();
    ASSERT_EQ((size_t) 1, trianglesOnRoot.size());
    EXPECT_TRUE(equal(trianglesOnRoot[0]->getNormal(), Vector3({0, 0, 1})));
    EXPECT_TRUE(equal(trianglesOnRoot[0]->getFirstPoint(), vertices[0]));
    EXPECT_TRUE(equal(viewer.getPlaneNormal(), Vector3({0, 0, 1})));
    EXPECT_TRUE(equal(viewer.getPlaneOrigin(), vertices[0]));
    EXPECT_FALSE(viewer.HasUnderChild());
    EXPECT_FALSE(viewer.HasOverChild());
}

TEST(BSPTree, OneUnderTriangle)
{
    std::vector<std::shared_ptr<ITriangle> > triangles;
    triangles.emplace_back(createTriangle(vertices, 0, 1, 2));
    triangles.emplace_back(createTriangle(vertices, 4, 5, 6));
    // BUILD use first the last triangle
    std::shared_ptr<BSPTree> pTree(BSPTree::buildBSPTree(triangles));
    ViewerBSPTree viewer(*pTree);
    
    const std::vector<std::shared_ptr<ITriangle> >& trianglesOnRoot = viewer.getTriangleOnRoot();
    ASSERT_EQ((size_t) 1, trianglesOnRoot.size());
    EXPECT_TRUE(equal(trianglesOnRoot[0]->getNormal(), Vector3({0, 0, 1})));
    EXPECT_TRUE(equal(trianglesOnRoot[0]->getFirstPoint(), vertices[4]));
    EXPECT_TRUE(equal(viewer.getPlaneNormal(), Vector3({0, 0, 1})));
    EXPECT_TRUE(equal(viewer.getPlaneOrigin(), vertices[4]));
    ASSERT_TRUE(viewer.HasUnderChild());
    EXPECT_FALSE(viewer.HasOverChild());
    ViewerBSPTree viewerUnder = viewer.getUnderBSPTree();
    const std::vector<std::shared_ptr<ITriangle> >& trianglesUnder = viewerUnder.getTriangleOnRoot();
    ASSERT_EQ((size_t) 1, trianglesUnder.size());
    EXPECT_TRUE(equal(trianglesUnder[0]->getNormal(), Vector3({0, 0, 1})));
    EXPECT_TRUE(equal(trianglesUnder[0]->getFirstPoint(), vertices[0]));
    EXPECT_TRUE(equal(viewerUnder.getPlaneNormal(), Vector3({0, 0, 1})));
    EXPECT_TRUE(equal(viewerUnder.getPlaneOrigin(), vertices[0]));
    EXPECT_FALSE(viewerUnder.HasUnderChild());
    EXPECT_FALSE(viewerUnder.HasOverChild());
}

TEST(BSPTree, OneOverTriangle)
{
    std::vector<std::shared_ptr<ITriangle> > triangles;
    triangles.emplace_back(createTriangle(vertices, 4, 5, 6));
    triangles.emplace_back(createTriangle(vertices, 0, 1, 2));
    // BUILD use first the last triangle
    std::shared_ptr<BSPTree> pTree(BSPTree::buildBSPTree(triangles));
    ViewerBSPTree viewer(*pTree);
    
    const std::vector<std::shared_ptr<ITriangle> >& trianglesOnRoot = viewer.getTriangleOnRoot();
    ASSERT_EQ((size_t) 1, trianglesOnRoot.size());
    EXPECT_TRUE(equal(trianglesOnRoot[0]->getNormal(), Vector3({0, 0, 1})));
    EXPECT_TRUE(equal(trianglesOnRoot[0]->getFirstPoint(), vertices[0]));
    EXPECT_TRUE(equal(viewer.getPlaneNormal(), Vector3({0, 0, 1})));
    EXPECT_TRUE(equal(viewer.getPlaneOrigin(), vertices[0]));
    EXPECT_FALSE(viewer.HasUnderChild());
    ASSERT_TRUE(viewer.HasOverChild());
    ViewerBSPTree viewerOver = viewer.getOverBSPTree();
    const std::vector<std::shared_ptr<ITriangle> >& trianglesOver = viewerOver.getTriangleOnRoot();
    ASSERT_EQ((size_t) 1, trianglesOver.size());
    EXPECT_TRUE(equal(trianglesOver[0]->getNormal(), Vector3({0, 0, 1})));
    EXPECT_TRUE(equal(trianglesOver[0]->getFirstPoint(), vertices[4]));
    EXPECT_TRUE(equal(viewerOver.getPlaneNormal(), Vector3({0, 0, 1})));
    EXPECT_TRUE(equal(viewerOver.getPlaneOrigin(), vertices[4]));
    EXPECT_FALSE(viewerOver.HasUnderChild());
    EXPECT_FALSE(viewerOver.HasOverChild());
}
