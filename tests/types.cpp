/*:*
  Copyright Jean-Denis Lesage (2011)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include "matrix.h"
#include "types.h"
#include "gtest/gtest.h"

using namespace raytrace;

TEST(Vector3, equal)
{
    Vector3 v1({0, 1, 2});
    Vector3 v2({0, 1, 2});
    Vector3 v3({0.01, 1, 2});

    EXPECT_TRUE(equal(v1, v2));
    EXPECT_FALSE(equal(v1, v3));
}

TEST(Vector3, ctor)
{
    Vector3 v1;

    EXPECT_TRUE(equal(v1, Vector3({0, 0, 0})));
}

TEST(Vector3, norm)
{
    Vector3 v1 = vecNull;
    Vector3 v2({1, 0, 0});
    Vector3 v3({0, -1, 0});
    Vector3 v4({-3, -5, sqrt(2.0)});

    EXPECT_TRUE(equalReal(v1.norm(), 0));
    EXPECT_TRUE(equalReal(v2.norm(), 1));
    EXPECT_TRUE(equalReal(v3.norm(), 1));
    EXPECT_TRUE(equalReal(v4.norm(), 6));
}

TEST(Vector3, div)
{
    Vector3 v({5, 2, -4});
    EXPECT_TRUE(equal(v / 3.0, Vector3({5.0/3.0, 2.0/3.0, -4.0/3.0})));
    v /= 3;
    EXPECT_TRUE(equal(v, Vector3({5.0/3.0, 2.0/3.0, -4.0/3.0})));
    EXPECT_TRUE(equal(3.0f * v, Vector3({5, 2, -4})));
}

TEST(Vector3, add)
{
    Vector3 v1({5, 2, -4});
    Vector3 v2({4, -2, 5});
    EXPECT_TRUE(equal(v1 + v2, Vector3({9, 0, 1})));
    EXPECT_TRUE(equal(v1 - v2, Vector3({1, 4, -9})));
}

TEST(Vector3, cross)
{
    Vector3 v1 = vecNull;
    Vector3 v2({1, 0, 0});
    Vector3 v3({0, 1, 0});
    EXPECT_TRUE(equal(vecNull, cross(v1, v1)));
    EXPECT_TRUE(equal(vecNull, cross(v1, v2)));
    EXPECT_TRUE(equal(vecNull, cross(v2, v2)));
    EXPECT_TRUE(equal(vecNull, cross(v2, v2/4.0)));
    EXPECT_TRUE(equal(Vector3({0, 0, 1}), cross(v2, v3)));
    EXPECT_TRUE(equal(Vector3({0, 0, -1}), cross(v3, v2)));
}

TEST(Vector3, dot)
{
    Vector3 v1({0, 0, 0});
    Vector3 v2({1, 0, 0});
    Vector3 v3({42, -5, 3});
    Vector3 v4({0.5, -1, 0});
    EXPECT_TRUE(equalReal(0.0f, dot(v1, v3)));
    EXPECT_TRUE(equalReal(42.0f, dot(v2, v3)));
    EXPECT_TRUE(equalReal(26.0f, dot(v3, v4)));
}

TEST(Vector4, equal)
{
    Vector4 v1({0, 1, 2, -4});
    Vector4 v2({0, 1, 2, -4});
    Vector4 v3({0.01, 1, 2, 6});

    EXPECT_TRUE(equal(v1, v2));
    EXPECT_FALSE(equal(v1, v3));
}

TEST(Vector4, ctor)
{
    Vector4 v1;

    EXPECT_TRUE(equal(v1, Vector4({0, 0, 0, 0})));
}

TEST(Vector4, norm)
{
    Vector4 v1;
    Vector4 v2({1, 0, 0, 0});
    Vector4 v3({0, -1, 0, 0});
    Vector4 v4({-3, -4, sqrt(2.0), 3});

    EXPECT_TRUE(equalReal(v1.norm(), 0));
    EXPECT_TRUE(equalReal(v2.norm(), 1));
    EXPECT_TRUE(equalReal(v3.norm(), 1));
    EXPECT_TRUE(equalReal(v4.norm(), 6));
}

TEST(Vector4, extractVec3)
{
    Vector4 v({4, 3, -1, 6});
    EXPECT_TRUE(equal(v.extractVector3(), Vector3({4, 3, -1})));
}

TEST(Vector4, div)
{
    Vector4 v({5, 2, -4, 8});
    EXPECT_TRUE(equal(v / 3.0, Vector4({5.0/3.0, 2.0/3.0, -4.0/3.0, 8.0/3.0})));
    v /= 3;
    EXPECT_TRUE(equal(v, Vector4({5.0/3.0, 2.0/3.0, -4.0/3.0, 8.0/3.0})));
    EXPECT_TRUE(equal(3.0f * v, Vector4({5, 2, -4, 8})));
}

TEST(Vector4, add)
{
    Vector4 v1({5, 2, -4, 7});
    Vector4 v2({4, -2, 5, -3});
    EXPECT_TRUE(equal(v1 + v2, Vector4({9, 0, 1, 4})));
    EXPECT_TRUE(equal(v1 - v2, Vector4({1, 4, -9, 10})));
}

TEST(Vector4, dot)
{
    Vector4 v1;
    Vector4 v2({1, 0, 0, 0});
    Vector4 v3({42, -5, 3, 10});
    Vector4 v4({0.5, -1, 0, -3.4});
    EXPECT_TRUE(equalReal(0.0f, dot(v1, v3)));
    EXPECT_TRUE(equalReal(42.0f, dot(v2, v3)));
    EXPECT_TRUE(equalReal(-8.0f, dot(v3, v4)));
}

TEST(Matrix44, equal)
{
    Matrix44 m1({0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15});
    Matrix44 m2({0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15});
    Matrix44 m3({0, 1, 2, 3, 4, 5, 6.1, 7, 8, 9, 10, 11, 12, 13, 14, 15});
    EXPECT_TRUE(equal(m1, m2));
    EXPECT_FALSE(equal(m1, m3));
}

TEST(Matrix44, ctor)
{
    Matrix44 m;
    EXPECT_TRUE(equal(m, Matrix44({ 1, 0, 0, 0, 
                                    0, 1, 0, 0, 
                                    0, 0, 1, 0,
                                    0, 0, 0, 1})));
}

TEST(Matrix44, det)
{
    Matrix44 id;
    EXPECT_TRUE(equalReal(1.0, id.det()));
    id(0,0) = 0.0f;
    EXPECT_TRUE(equalReal(0.0, id.det()));
    Matrix44 misc({1, 2, 3, 9, -3, 4, 5, -2, 6, 7, -1, 1, 3, 4, -3, 1});
    EXPECT_TRUE(equalReal(-1206, misc.det()));
    Vector3 Y({0,0,1}); 
    Vector3 Z({1,1,0});
    Vector3 X({-1,1,0});
    Z /= Z.norm();
    X /= X.norm();
    Matrix44 tmp({X[0], Y[0], Z[0], 1,
            X[1], Y[1], Z[1], 0,
            X[2], Y[2], Z[2], -1,
            0, 0, 0, 1});
    EXPECT_TRUE(equalReal(1.0, tmp.det()));
}

TEST(Matrix44, inv)
{
    Matrix44 id;
    EXPECT_TRUE(equal(id, id.inv()));
    EXPECT_TRUE(equal(rotation(Vector3({1, 0, 0}), 1.4).inv(), rotation(Vector3({1, 0, 0}), -1.4)));
    EXPECT_TRUE(equal(scale(2.4, -2.1, 1.4).inv(), scale(1.0/2.4, -1.0/2.1, 1.0/1.4)));
}

TEST(Matrix44, InvMultMIsIdentity)
{
    Vector3 Y({0,0,1}); 
    Vector3 Z({1,1,0});
    Vector3 X({-1,1,0});
    Z /= Z.norm();
    X /= X.norm();
    Matrix44 tmp({X[0], Y[0], Z[0], 1,
            X[1], Y[1], Z[1], 0,
            X[2], Y[2], Z[2], -1,
            0, 0, 0, 1});
    Matrix44 identity;
    EXPECT_TRUE(equal(identity, tmp.inv()*tmp)); 
}

TEST(Matrix44, mult)
{
    Matrix44 m1({0, 1, 2, 3, 
                 4, 5, 6, 7,
                 8, 9, 10, 11,
                 12, 13, 14, 15});
    Matrix44 m2({1, 1, 1, 1, 
                 1, 1, 1, 1,
                 1, 1, 1, 1,
                 1, 1, 1, 1});
    Matrix44 m3({6, 6, 6, 6, 
                 22, 22, 22, 22,
                 38, 38, 38, 38,
                 54, 54, 54, 54});
    Vector4 v1({1, 1, 1, 1});

    EXPECT_TRUE(equal(m1 * Matrix44(), m1));
    EXPECT_TRUE(equal(m1 * m2, m3));
    EXPECT_TRUE(equal(Matrix44() * v1, v1));
    EXPECT_TRUE(equal(m2 * v1, 4*v1));
    EXPECT_TRUE(equal(m1 * v1, Vector4({6, 22, 38, 54})));
}

TEST(Matrix44, add)
{
    Matrix44 m1({0, 1, 2, 3, 
                 4, 5, 6, 7,
                 8, 9, 10, 11,
                 12, 13, 14, 15});
    Matrix44 m2({1, 1, 1, 1, 
                 1, 1, 1, 1,
                 1, 1, 1, 1,
                 1, 1, 1, 1});
    Matrix44 m3({1, 2, 3, 4,
               5, 6, 7, 8,
               9, 10, 11, 12,
               13, 14, 15, 16});
    Matrix44 m4({-1, 0, 1, 2,
               3, 4, 5, 6,
               7, 8, 9, 10,
               11, 12, 13, 14});

    EXPECT_TRUE(equal(m1 + m2, m3));
    EXPECT_TRUE(equal(m1 - m2, m4));
}

TEST(Matrix33, equal)
{
    Matrix33 m1({0, 1, 2, 3, 4, 5, 6, 7, 8});
    Matrix33 m2({0, 1, 2, 3, 4, 5, 6, 7, 8});
    Matrix33 m3({0, 1, 2, 3, 4, 5, 6.1, 7, 8});
    EXPECT_TRUE(equal(m1, m2));
    EXPECT_FALSE(equal(m1, m3));
}

TEST(Matrix33, ctor)
{
    Matrix33 m;
    EXPECT_TRUE(equal(m, Matrix33({ 1, 0, 0, 
                                    0, 1, 0, 
                                    0, 0, 1})));
}

TEST(Matrix33, mult)
{
    Matrix33 m1({0, 1, 2, 
                 4, 5, 6,
                 8, 9, 10});

    Matrix33 m2({0, 2, 4, 
                 8, 10, 12,
                 16, 18, 20});

    EXPECT_TRUE(equal(m1 * 0.0f, Matrix33({0, 0, 0, 0, 0, 0, 0, 0, 0})));
    EXPECT_TRUE(equal(m1 * 1.0f, m1));
    EXPECT_TRUE(equal(2.0f * m1, m2));
}

TEST(Matrix33, add)
{
    Matrix33 m1({0, 1, 2, 
                 4, 5, 6,
                 8, 9, 10});
    Matrix33 m2({1, 1, 1, 
                 1, 1, 1,
                 1, 1, 1});
    Matrix33 m3({1, 2, 3,
               5, 6, 7,
               9, 10, 11});
    Matrix33 m4({-1, 0, 1,
               3, 4, 5,
               7, 8, 9});

    EXPECT_TRUE(equal(m1 + m2, m3));
    EXPECT_TRUE(equal(m1 - m2, m4));
}
