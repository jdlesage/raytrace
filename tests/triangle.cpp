/**
  Copyright Jean-Denis Lesage (2011)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include <boost/assign.hpp>
#include <boost/cast.hpp>
#include <memory>
#include "compareImage.h"
#include "scene.h"
#include "matrix.h"
#include "gtest/gtest.h"
#include "triangleFactory.h"

using namespace raytrace;

const std::vector<Vector3> vertices = boost::assign::list_of
                                        (Vector3({-1, -1, 0}))
                                        (Vector3({1, -1, 0}))
                                        (Vector3({0, 1, 0}));


TEST(Triangle, IntersectTriangleSuccess)
{
    std::unique_ptr<Object> pt(boost::polymorphic_cast<Object*>(createTriangle(vertices, 0, 1, 2)));
    Ray r(Vector4({0, 0, 1, 1}), Vector4({0, 0, -1, 0}));
    EXPECT_TRUE(pt->intersect(r).impact);
}

TEST(Triangle, GetNormal)
{
    std::unique_ptr<ITriangle> pt(createTriangle(vertices, 0, 1, 2));
    EXPECT_TRUE(equal(pt->getNormal(), Vector3({0, 0, 1})));
}

TEST(Triangle, GetFirstPoint)
{
    std::unique_ptr<ITriangle> pt(createTriangle(vertices, 0, 1, 2));
    EXPECT_TRUE(equal(pt->getFirstPoint(), vertices[0]));

}

TEST(Triangle, IntersectTriangleFailed)
{
    std::unique_ptr<Object> pt(boost::polymorphic_cast<Object*>(createTriangle(vertices, 0, 1, 2)));
    Ray r(Vector4({1, 1, 1, 1}), Vector4({0, 0, -1, 0}));
    EXPECT_FALSE(pt->intersect(r).impact);
}

TEST(Triangle, IntersectTriangleDistance)
{
    std::unique_ptr<Object> pt(boost::polymorphic_cast<Object*>(createTriangle(vertices, 0, 1, 2)));
    Ray r(Vector4({0, 0, 1.45, 1}), Vector4({0, 0, -1, 0}));
    EXPECT_NEAR(1.45, pt->intersect(r).distance, g_tolerance);
}

TEST(Triangle, IntersectTriangleNormal)
{
    std::unique_ptr<Object> pt(boost::polymorphic_cast<Object*>(createTriangle(vertices, 0, 1, 2)));
    Ray r(Vector4({0, 0, 1, 1}), Vector4({0, 0, -1, 0}));
    EXPECT_TRUE(equal(Vector3({0, 0, 1}), pt->intersect(r).normal));
}

TEST(Triangle, IntersectTriangleNormalTranslate)
{
    std::unique_ptr<Object> pt(boost::polymorphic_cast<Object*>(createTriangle(vertices, 0, 1, 2)));
    pt->setTransformation(translation(-5, 3, -10));
    Ray r(Vector4({-5, 2.5, 1, 1}), Vector4({0, 0, -1, 0}));
    EXPECT_NEAR(11, pt->intersect(r).distance, g_tolerance); 
}

TEST(Triangle, RenderTriangle)
{
    std::shared_ptr<Object> pTriangle(boost::polymorphic_cast<Object*>(createTriangle(vertices, 0, 1, 2)));
    Scene scene;
    scene.addObject(pTriangle);
    QImage result = scene.render();
    EXPECT_TRUE(compareImage(result, "triangle.png"));
}

TEST(Triangle, TestOverPlane)
{
    std::unique_ptr<ITriangle> pt(createTriangle(vertices, 0, 1, 2));

    Vector3 origin({0, 0, 4});
    Vector3 normal({0, 0, -1});
    EXPECT_EQ(ITriangle::IS_OVER_PLANE, pt->getPositionFollowingPlane(normal, origin));
}

TEST(Triangle, TestUnderPlane)
{
    std::unique_ptr<ITriangle> pt(createTriangle(vertices, 0, 1, 2));

    Vector3 origin({0, 0, 4});
    Vector3 normal({0, 0, 1});
    EXPECT_EQ(ITriangle::IS_UNDER_PLANE, pt->getPositionFollowingPlane(normal, origin));
}

TEST(Triangle, TestOnPlane)
{
    std::unique_ptr<ITriangle> pt(createTriangle(vertices, 0, 1, 2));

    EXPECT_EQ(ITriangle::IS_ON_PLANE, pt->getPositionFollowingPlane(pt->getNormal(), pt->getFirstPoint()));


}
TEST(Triangle, TestIntersectPlane)
{
    std::unique_ptr<ITriangle> pt(createTriangle(vertices, 0, 1, 2));

    Vector3 origin({0, 0, 0});
    Vector3 normal({1, 1, 0});
    EXPECT_EQ(ITriangle::INTERSECT_PLANE, pt->getPositionFollowingPlane(normal, origin));
}
