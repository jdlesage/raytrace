/**
  Copyright Jean-Denis Lesage (2012)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#pragma once

#include "types.h"
#include "object.h"

namespace raytrace
{
    class ITriangle : public Object
    {
        public:
            typedef enum{
                IS_OVER_PLANE,
                IS_UNDER_PLANE,
                IS_ON_PLANE,
                INTERSECT_PLANE
            } PlanePosition;

            ITriangle(): Object() {}
            virtual ~ITriangle() {}
            virtual Vector3 getNormal() const =0;
            virtual Vector3 getFirstPoint() const =0;
            virtual Vector3 getSecondPoint() const =0;
            virtual Vector3 getThridPoint() const =0;
            virtual PlanePosition getPositionFollowingPlane(const Vector3& planeNormal, const Vector3& planePosition) const =0;
            virtual ITriangle* clone() const = 0;
    };
}
