/**
  Copyright Jean-Denis Lesage (2012)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include <initializer_list>
#include <iostream>
#include <math.h>
#include <stdint.h>
#include <xmmintrin.h>

#pragma once

namespace raytrace
{
    typedef float Real_t;
#ifndef __SSE4_1__
    const Real_t g_tolerance = 1e-5;
#else
    const Real_t g_tolerance = 1e-2;
#endif 
    inline bool equalReal(Real_t a, Real_t b) {return fabsf(a - b) < g_tolerance;}

    class Vector3
    {
        public:
            // ctor = undefined vector
            Vector3(){}
            Vector3(const std::initializer_list<float>& list);
            Vector3(float x_, float y_, float z_) : x(x_), y(y_), z(z_) {}

            float norm() const;
            Vector3 operator/(float value) const;
            Vector3& operator/=(float value);
            Vector3 operator+(const Vector3& other) const;
            Vector3 operator-(const Vector3& other) const;
            Vector3 operator-() const;
            float& operator[] (const size_t value);
            float operator[] (const size_t value) const;

        protected:
            union {
                struct {float x, y, z;};
                __m128 v;
            };
            friend Vector3 cross(const Vector3& v1, const Vector3& v2);
            friend float dot(const Vector3& v1, const Vector3& v2);
            friend Vector3 operator*(const Vector3& vec, float scalar);
            friend bool equal(const Vector3& vec1,  const Vector3& vec2);
            friend std::ostream& operator<<(std::ostream& out, const Vector3& vec);

    };
    Vector3 cross(const Vector3& v1, const Vector3& v2);
    float dot(const Vector3& v1, const Vector3& v2);
    Vector3 operator*(const Vector3& vec, float scalar);
    inline Vector3 operator*(float scalar, const Vector3& vec)
    {
        return vec * scalar;
    }
    bool equal(const Vector3& vec1,  const Vector3& vec2);
    std::ostream& operator<<(std::ostream& out, const Vector3& vec);

    const Vector3 vecNull = Vector3(0, 0, 0);

    class Matrix44;

    // Friend
    class Ray;

    class Vector4
    {
        public:
            // ctor = vector null
            Vector4();
            Vector4(const std::initializer_list<float>& list);

            Vector3 extractVector3() const {return Vector3(x, y, z);}
            float norm() const;
            Vector4 operator/(float value) const;
            Vector4& operator/=(float value);
            Vector4 operator+(const Vector4& other) const;
            Vector4 operator-(const Vector4& other) const;
            Vector4 operator-() const;
            float& operator[] (const size_t value);
            float operator[] (const size_t value) const;

        protected: 
            union {
                struct {float x, y, z, w;};
                __m128 v;
            };

            friend float dot(const Vector4& v1, const Vector4& v2);
            friend Vector4 operator*(const Vector4& vec, float scalar);
            friend Vector4 operator*(const Matrix44& mat, const Vector4& vec);
            friend bool equal(const Vector4& vec1,  const Vector4& vec2);
            friend class Ray;
    };
    float dot(const Vector4& v1, const Vector4& v2);
    Vector4 operator*(const Vector4& vec, float scalar);
    inline Vector4 operator*(float scalar, const Vector4& vec)
    {
        return vec * scalar;
    }        
    bool equal(const Vector4& vec1,  const Vector4& vec2);


    class Matrix44
    {
        public :
            // ctor = matrix identity
            Matrix44();
            Matrix44(const std::initializer_list<float>& list);

            Matrix44 inv() const;
            Real_t det() const;
            Matrix44 operator*(const Matrix44& other) const;
            Matrix44 operator+(const Matrix44& other) const;
            Matrix44 operator-(const Matrix44& other) const;
            float& operator()(size_t i, size_t j);
        protected:
            Vector4 row[4];

            friend Vector4 operator*(const Matrix44& mat, const Vector4& vec);
            friend bool equal(const Matrix44& mat1,  const Matrix44& mat2);
            friend std::ostream& operator<<(std::ostream& out, const Matrix44& mat);
    };

    Vector4 operator*(const Matrix44& mat, const Vector4& vec);
    bool equal(const Matrix44& mat1,  const Matrix44& mat2);
    std::ostream& operator<<(std::ostream& out, const Matrix44& mat);

    class Matrix33
    {
        public :
            // ctor = matrix identity
            Matrix33();
            Matrix33(const std::initializer_list<float>& list);

            Matrix33 operator+(const Matrix33& other) const;
            Matrix33 operator-(const Matrix33& other) const;
            float& operator()(size_t i, size_t j);
        protected:
            Vector3 row[3];

            friend Matrix33 operator*(const Matrix33& mat, float scalar);
            friend bool equal(const Matrix33& mat1,  const Matrix33& mat2);
    };


        Matrix33 operator*(const Matrix33& mat, float scalar);
        inline Matrix33 operator*(float scalar, const Matrix33& mat)
        {
            return mat*scalar;
        }
        bool equal(const Matrix33& mat1,  const Matrix33& mat2);
}
