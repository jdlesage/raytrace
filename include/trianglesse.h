/**
  Copyright Jean-Denis Lesage (2012)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#pragma once

#include "iTriangle.h"
#include "object.h"
#include <smmintrin.h>
#include <vector>

namespace raytrace
{
    class TriangleSSE :  public ITriangle
    {
        public :
            TriangleSSE(const std::vector<Vector3>& vertexBuffer, size_t a, size_t b, size_t c);
            virtual ~TriangleSSE() {}
            virtual void setTransformation(const Matrix44& transformationMatrix);
            virtual Vector3 getNormal() const {return m_vecN;};
            virtual Vector3 getFirstPoint() const {return m_a;}
            virtual Vector3 getSecondPoint() const {return m_b;}
            virtual Vector3 getThridPoint() const {return m_c;}
            virtual PlanePosition getPositionFollowingPlane(const Vector3& planeNormal, const Vector3& planePosition) const;
            virtual Impact intersect(const Ray& ray) const {return intersectInObjectSpace(ray);}
            virtual ITriangle* clone() const;
        protected:
            TriangleSSE(const TriangleSSE& t);
            virtual Impact intersectInObjectSpace(const Ray& ray) const;
            void computeConstants();

            __m128 m_u;
            __m128 m_v;
            __m128 m_n;
            Vector3 m_vecN;
            Vector3 m_a;
            Vector3 m_b;
            Vector3 m_c;
            Vector3 m_origA;
            Vector3 m_origB;
            Vector3 m_origC;
            float m_d;
    };
}
