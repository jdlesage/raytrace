/**
  Copyright Jean-Denis Lesage (2012)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#pragma once

#include <memory>
#include "bspTree.h"
#include "iprimitivecontainer.h"
#include "primitiveContainer.h"

namespace raytrace
{

    class BSPPrimitiveContainer : public IPrimitiveContainer
    {
        public:
            BSPPrimitiveContainer();
            virtual ~BSPPrimitiveContainer() {}

            virtual void addObject(std::shared_ptr<Object>& o);
            virtual size_t size() const; 
            virtual void clear();
            virtual void findFirstImpact(const std::vector<Ray>& rays, std::vector<Impact>& impacts) const;
            virtual void lock();
        protected:
            PrimitiveContainer m_nonTriangleContainer;
            std::unique_ptr<BSPTree> m_pTree;
            std::vector<std::shared_ptr<ITriangle> > m_trianglesToInsertInBSP;
            std::vector<std::shared_ptr<Object> > m_objectContainer;
    };
}
