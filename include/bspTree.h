/**
  Copyright Jean-Denis Lesage (2012)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#pragma once

#include <limits>
#include <memory>
#include <vector>
#include "iTriangle.h"
#include "object.h"
#include "ray.h"
#include "types.h"

namespace raytrace
{
    class ViewerBSPTree;

    class BSPTree
    {
        public:
            BSPTree(std::shared_ptr<ITriangle>& triangleReference);
            bool isValid() const;
            
            static BSPTree* buildBSPTree(std::vector<std::shared_ptr<ITriangle> >& triangles);
            Impact intersect(const Ray& ray, const std::pair<Real_t, Real_t>& limits = std::pair<Real_t, Real_t>(0.0f, std::numeric_limits<Real_t>::max())) const;
        
        protected:
            Impact intersectTriangleAtLevel(const Ray& ray) const;

            std::vector<std::shared_ptr<ITriangle> >m_trianglesOnPlane;
            std::unique_ptr<BSPTree> m_pOverTriangles;
            std::unique_ptr<BSPTree> m_pUnderTriangles;
            Vector3 m_planeNormal;
            Vector3 m_planeOrigin;

            friend class ViewerBSPTree;
    };

    /* A class to be able to view value of the tree. (debug and test
     * purporses)*/
    class ViewerBSPTree
    {
        public:
            ViewerBSPTree(const BSPTree& tree): m_tree(tree) {}

            const std::vector<std::shared_ptr<ITriangle> >& getTriangleOnRoot() const {return m_tree.m_trianglesOnPlane;}
            Vector3 getPlaneNormal() const {return m_tree.m_planeNormal;}
            Vector3 getPlaneOrigin() const {return m_tree.m_planeOrigin;}
            bool HasOverChild() const {return m_tree.m_pOverTriangles.get() != NULL;}
            bool HasUnderChild() const {return m_tree.m_pUnderTriangles.get() != NULL;}
            ViewerBSPTree getOverBSPTree() const;
            ViewerBSPTree getUnderBSPTree() const;
        protected:
            const BSPTree& m_tree;
    };
}
