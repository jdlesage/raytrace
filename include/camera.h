/**
  Copyright Jean-Denis Lesage (2012)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#pragma once

#include <utility>
#include "types.h"
#include "ray.h"

namespace raytrace
{

    class Camera
    {
        public:
            Camera();
            virtual ~Camera() {}

            const Vector3& getPosition() const {return m_position;}
            const Vector3& getTarget() const {return m_target;}
            const Vector3& getUp() const {return m_up;}

            void setPosition(const Vector3& position);
            void setTarget(const Vector3& target);
            void setUp(const Vector3& up);
            
            const Matrix44& invViewMatrix() const {return m_invViewMatrix;}

            void setFrustrum(   Real_t left,
                                Real_t right,
                                Real_t bottom,
                                Real_t top,
                                Real_t near,
                                Real_t far);
            
            const Matrix44& invProj() const {return m_invProj;}

            void setViewPort(uint32_t width, uint32_t height);
            std::pair<uint32_t, uint32_t> getViewPort() const;

            Ray getRayFromPixel(uint32_t x, uint32_t y) const;

        protected:
            void computeInvViewMatrix();

            Matrix44 m_invProj;
            Matrix44 m_invViewMatrix;
            Matrix44 m_invViewMatrixProj;

            Vector3 m_position;  /// Camera position
            Vector3 m_target;    /// Camera target. Assert norm=1
            Vector3 m_up;       /// Camera up vector Assert nomr=1

            // Frustrum
            Real_t m_left;
            Real_t m_right;
            Real_t m_bottom;
            Real_t m_top;
            Real_t m_near;
            Real_t m_far;

            //View port
            uint32_t m_width;
            uint32_t m_height;
    };
}
