SET(raytracer_src
    bspPrimitiveContainer.cpp
    bspTree.cpp
    camera.cpp
    light.cpp
    matrix.cpp
    mesh.cpp
    mixPrimitiveContainer.cpp
    object.cpp
    primitiveContainer.cpp
    ray.cpp
    scene.cpp
    sphere.cpp
    triangle.cpp
    triangleFactory.cpp
    trianglesse.cpp
    types.cpp)

set(CL_src
    CL/CLPrimitiveContainer.cpp
    CL/triangleIntersector.cpp)

ADD_LIBRARY(raytrace SHARED ${raytracer_src} ${CL_src})

TARGET_LINK_LIBRARIES(raytrace
                    ${Boost_LIBRARIES}
                    ${OPENCL_LIBRARIES}
                    ${QT_LIBRARIES})

INSTALL(TARGETS raytrace LIBRARY DESTINATION lib)
INSTALL(FILES CL/triangleIntersector.cl DESTINATION share)
