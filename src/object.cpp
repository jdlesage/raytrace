/**
  Copyright Jean-Denis Lesage (2012)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include <assert.h> 
#include "object.h"

namespace raytrace
{
    Object::Object() : m_invTransformationMatrix(), m_transformationMatrix()
    {
        for(uint8_t i = 0; i != 4; ++i)
            for(uint8_t j = 0; j != 4; ++j)
            {
                m_invTransformationMatrix(i, j) = (i == j) ? 1 : 0;
                m_transformationMatrix(i, j) = (i == j) ? 1 : 0;
            }
    }
    
            
    void Object::setTransformation(const Matrix44& transformationMatrix)
    {
        m_invTransformationMatrix = transformationMatrix.inv();
        m_transformationMatrix = transformationMatrix;
    }

    Impact Object::intersect(const Ray& ray) const
    {
        Impact result = intersectInObjectSpace(m_invTransformationMatrix * ray);
        if(result.impact)
        {
            // Return results in world basis:
            // Distance:
            result.distance /= ray.getNormDirectionAfterTransformation(m_invTransformationMatrix);
            // Normal:
            Vector4 normal({result.normal[0], result.normal[1], result.normal[2], 0.0});
            normal = m_transformationMatrix * normal;
            assert(equalReal(normal[3], 0.0));
            assert(!equalReal(normal.norm(), 0.0f));
            result.normal = normal.extractVector3();
            result.normal /= result.normal.norm();
        }
        return result;
    }
}
