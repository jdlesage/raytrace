#include <assert.h> 
#include "matrix.h"

namespace raytrace
{
    Matrix44 translation(Real_t x, Real_t y, Real_t z)
    {
        Matrix44 temp({1, 0, 0, x, 
                0, 1, 0, y, 
                0 ,0, 1, z,
                0, 0, 0, 1});
        return temp;
    }    

    Matrix44 scale(Real_t x, Real_t y, Real_t z)
    {
        Matrix44 temp({x, 0, 0, 0, 
                0, y, 0, 0, 
                0 ,0, z, 0,
                0, 0, 0, 1});

        return temp;
    }    


    Matrix44 rotation(const Vector3& axe, Real_t angle)
    {
        assert(!equalReal(axe.norm(),0));
        const Vector3 axeNorm = axe / axe.norm();
        Matrix33 tensorProduct({axeNorm[0]*axeNorm[0], axeNorm[0]*axeNorm[1], axeNorm[0]*axeNorm[2],
                axeNorm[1]*axeNorm[0], axeNorm[1]*axeNorm[1], axeNorm[1]*axeNorm[2],
                axeNorm[2]*axeNorm[0], axeNorm[2]*axeNorm[1], axeNorm[2]*axeNorm[2]});
        Matrix33 crossProduct({0, axeNorm[2], -axeNorm[1],
                -axeNorm[2], 0, axeNorm[0],
                axeNorm[1], -axeNorm[0], 0});
        Matrix33 id;
        Matrix33 tmp =  id*cos(angle) + sin(angle) * crossProduct + (1 - cos(angle)) * tensorProduct;
        Matrix44 result;
        for(uint32_t i = 0; i != 3; ++i)
        {
            for(uint32_t j = 0; j != 3; ++j)
            {
                result(i,j) = tmp(i,j);
            }
        }
        result(0,3) = 0;
        result(1,3) = 0;
        result(2,3) = 0;
        result(3,0) = 0;
        result(3,1) = 0;
        result(3,2) = 0;
        result(3,3) = 1;
        return result;
    }

    Matrix44 perspective(Real_t left, Real_t right, Real_t bottom, Real_t top, Real_t near, Real_t far)
    {
        assert(!equalReal(right, left));
        assert(!equalReal(top, bottom));
        assert(!equalReal(near, far));
        Matrix44 temp({2*near/(right - left), 0, (right + left)/(left - right), 0, 
                0, 2*near/(top - bottom), (top + bottom)/(bottom - top), 0, 
                0 ,0, (far + near)/(far - near), 2*far*near/(near - far),
                0, 0, 1, 0});
        return temp;
    }
}
