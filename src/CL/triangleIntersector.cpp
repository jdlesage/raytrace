/**
  Copyright Jean-Denis Lesage (2012)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include "CL/triangleIntersector.h"
#include <fstream> 
#include <iostream> 
#include <boost/filesystem.hpp> 
#include <boost/format.hpp> 
#define CL_USE_DEPRECATED_OPENCL_1_1_APIS
#include <CL/cl.hpp>

using namespace boost::filesystem;

namespace raytrace
{
    namespace CL
    {

        namespace
        {
            class CLException : public std::exception
            {
                public:
                    CLException(const std::string& message) : m_message(message) {}
                    virtual const char* what() const throw()
                    {
                        return m_message.c_str();
                    }
                    std::string m_message;
            };


            inline void logErr(const std::string& message)
            {

                const std::string error =  (boost::format("ERROR %s ") % message).str();
#ifdef LOG
                std::cerr << error << std::endl;
#endif //LOG
                throw CLException(error);
            }

            inline void logErr(cl_int err, const char * name)
            {
                std::string error;
                switch (err)
                {
                    case CL_SUCCESS:
                        break;
                    case CL_BUILD_PROGRAM_FAILURE:
                        error = (boost::format("ERROR : kernel compilation failed\n %s") % name).str();
                        break;
                    case CL_INVALID_BINARY:
                        error = (boost::format("ERROR %s : %s") % name % "The program binary is not a valid binary for the specified device. ").str();
                        break;
                    case CL_INVALID_BUFFER_SIZE:
                        error = (boost::format("ERROR %s : %s") % name % "The value of the buffer size is 0 or is greater than CL_DEVICE_MAX_MEM_ALLOC_SIZE for all devices specified in the parameter context. ").str();
                        break;
                    case CL_INVALID_KERNEL_ARGS:
                        error = (boost::format("ERROR %s : a kernel argument values have not been specified.\n") % name).str();
                        break;
                    default:
                        {
                            error =  (boost::format("ERROR %s : %d") % name % err).str();
                        }
                }

                if(err != CL_SUCCESS)
                {
#ifdef LOG
                    std::cerr << error << std::endl;
#endif //LOG
                    throw CLException(error);
                }
            }

            path findOpenCLKernelsSource(const std::string& fileName)
            {
                std::vector<path> potentialLocation;
                char* pPath = getenv("RAYTRACE_DIR");
                if(pPath)
                {
                    path location(pPath);
                    location /= "share";
                    location /= fileName;
                    potentialLocation.push_back(location);
                }

                path location = "/usr/share";
                location /= fileName;
                potentialLocation.push_back(location);

                location = "/usr/local/share";
                location /= fileName;
                potentialLocation.push_back(location);

                std::vector<path>::const_iterator it = std::find_if(potentialLocation.begin(), potentialLocation.end(), [](const path& p) {return exists(p);});

                if(it != potentialLocation.end())
                    return *it;
                return path("");
            }

            template<class T>
            T roundUp(const T& num, const T& den)
            {
                assert(den != 0);
                const float f = static_cast<float>(num) / static_cast<float>(den);
                return static_cast<T>(ceilf(f))*den;
            }

            template <class T>
                struct OpenCLBuffer
                {
                    std::unique_ptr<T[]> m_memory;
                    cl::Buffer m_buffer;
                    size_t m_size;

                    void alloc(size_t size)
                    {
                        if(size > 0)
                            m_memory.reset(new T[size]);
                        else
                            m_memory.reset();
                        m_size = size;
                    }

                    size_t size() const
                    {
                        return m_size * sizeof(T);
                    }
                };

            Vector3& assign(Vector3& vec, const cl_float3& cl_vec)
            {
                vec = Vector3(cl_vec.s[0], cl_vec.s[1], cl_vec.s[2]);
                return vec;
            }

            cl_float3& assign(cl_float3& cl_vec, const Vector3& vec)
            {
                cl_vec.s[0] = vec[0];
                cl_vec.s[1] = vec[1];
                cl_vec.s[2] = vec[2];
                return cl_vec;
            }

            class OpenCLImpl
            {
                public:
                    OpenCLImpl() 
                    {
                        if(m_nbContexts > 0)
                            throw CLException("Cannot create more than one context");

                        // Initialize devices and context
                        cl_int err;
                        std::vector< cl::Platform > platformList;
                        cl::Platform::get(&platformList);
                        logErr(platformList.size()!=0 ? CL_SUCCESS : -1, "cl::Platform::get");
#ifdef LOG
                        std::string platformVendor;
                        platformList[1].getInfo((cl_platform_info)CL_PLATFORM_VENDOR, &platformVendor);
                        std::cout << "PlatformVendor = " << platformVendor << std::endl;
#endif //LOG

                        cl_context_properties cprops[3] =
                        {CL_CONTEXT_PLATFORM, (cl_context_properties)(platformList[0])(), 0};
                        m_context = cl::Context(CL_DEVICE_TYPE_GPU, cprops, NULL, NULL, &err);
                        logErr(err, "Conext::Context()"); 
                        m_devices = m_context.getInfo<CL_CONTEXT_DEVICES>();
                        logErr(m_devices.size() > 0 ? CL_SUCCESS : -1, "devices.size() > 0");
#ifdef LOG
                        std::string deviceName;
                        m_devices[0].getInfo((cl_device_info)CL_DEVICE_NAME, &deviceName);
                        std::cout << "DeviceName = " << deviceName << std::endl;
#endif //LOG
                        ++m_nbContexts;
                        // Create a command queue and use the first device
                        m_queue = cl::CommandQueue(m_context, m_devices[0]);

                        // Compile kernel
                        const path kernelPath = findOpenCLKernelsSource("triangleIntersector.cl");
                        if(!exists(kernelPath))
                        {
                            logErr(std::string("Cannot find kernel source triangleIntersector.cl"));
                        }
                        std::ifstream file(kernelPath.string().c_str());
                        logErr(file.is_open() ? CL_SUCCESS:-1, "/home/jdlesage/raytrace/git/src/CL/triangleIntersector.cl");
                        std::string prog(std::istreambuf_iterator<char>(file), (std::istreambuf_iterator<char>()));

                        cl::Program::Sources source(1, std::make_pair(prog.c_str(), prog.length()+1));

                        cl::Program program(m_context, source);
                        err = program.build(m_devices,"");
                        logErr(err, program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(m_devices[0]).c_str());
                        m_kernel = cl::Kernel(program, "intersectRays", &err);
                        logErr(err, "CompileKernel"); 
                    }

                    template<class T>
                        cl_int uploadBuffer(size_t arg, OpenCLBuffer<T>& buffer)
                        {
                            cl_int err;
                            buffer.m_buffer = cl::Buffer(m_context, CL_MEM_READ_ONLY|CL_MEM_ALLOC_HOST_PTR, buffer.size(), NULL, &err);
                            err = m_queue.enqueueWriteBuffer(buffer.m_buffer, CL_TRUE, 0, buffer.size(), buffer.m_memory.get());
                            if(err != CL_SUCCESS)
                                return err;
                            err = m_kernel.setArg(arg, buffer.m_buffer);
                            return err;
                        }


                    void transferRay(const std::vector<Ray>& rays)
                    {
                        m_raySources.alloc(rays.size());
                        m_rayDir.alloc(rays.size());
                        for(size_t i = 0; i != rays.size(); ++i)
                        {
                            const Vector3 source = rays[i].getSource();
                            const Vector3 dir = rays[i].getDirection();
                            assign(m_raySources.m_memory[i], source);
                            assign(m_rayDir.m_memory[i], dir);
                        }
                        cl_int err;
                        err = uploadBuffer(3, m_raySources);
                        logErr(err, "LoadRaySourceBuffer");
                        err = uploadBuffer(4, m_rayDir);
                        logErr(err, "LoadRayDirBuffer");
                    }


                    void transferTriangles(const std::vector<std::shared_ptr<ITriangle> >& triangles)
                    {
                        m_trianglesA.alloc(triangles.size());
                        m_trianglesB.alloc(triangles.size());
                        m_trianglesC.alloc(triangles.size());
                        for(size_t i = 0; i != triangles.size(); ++i)
                        {
                            const Vector3 ptA = triangles[i]->getFirstPoint();
                            const Vector3 ptB = triangles[i]->getSecondPoint();
                            const Vector3 ptC = triangles[i]->getThridPoint();
                            assign(m_trianglesA.m_memory[i], ptA);
                            assign(m_trianglesB.m_memory[i], ptB);
                            assign(m_trianglesC.m_memory[i], ptC);
                        }
                        cl_int err;
                        err = uploadBuffer(0, m_trianglesA);
                        logErr(err, "LoadTriangleABuffer");
                        err = uploadBuffer(1, m_trianglesB);
                        logErr(err, "LoadTriangleBBuffer");
                        err = uploadBuffer(2, m_trianglesC);
                        logErr(err, "LoadTriangleCBuffer");
                        
                        int argSize = static_cast<int>(triangles.size());
                        err = m_kernel.setArg(9, sizeof(int), &argSize);
                        logErr(err, "setArgNTriangle");
                    }

                    void createOutputBuffers(size_t size)
                    {
                        m_impacts.alloc(size);
                        m_distances.alloc(size);
                        m_normal.alloc(size);
                        cl_int err;
                        m_impacts.m_buffer = cl::Buffer(m_context, CL_MEM_WRITE_ONLY|CL_MEM_ALLOC_HOST_PTR, m_impacts.size(), NULL, &err);
                        logErr(err, "CreateImpactBuffer");
                        err = m_kernel.setArg(5, m_impacts.m_buffer);
                        logErr(err, "setArgImpact");
                        m_distances.m_buffer = cl::Buffer(m_context, CL_MEM_WRITE_ONLY|CL_MEM_ALLOC_HOST_PTR, m_distances.size(), NULL, &err);
                        logErr(err, "CreateDistanceBuffer");
                        err = m_kernel.setArg(6, m_distances.m_buffer);
                        logErr(err, "setArgDistance");
                        m_normal.m_buffer = cl::Buffer(m_context, CL_MEM_WRITE_ONLY|CL_MEM_ALLOC_HOST_PTR, m_normal.size(), NULL, &err);
                        logErr(err, "CreateNormalBuffer");
                        err = m_kernel.setArg(7, m_normal.m_buffer);
                        logErr(err, "setArgNormal");
                    }

                    void compute(size_t size, std::vector<Impact>& impacts)
                    {
                        cl::NDRange global(roundUp<size_t>(size,512));
                        cl::NDRange local(512);
                        cl_int err;
                        int argSize = static_cast<int>(size);
                        err = m_kernel.setArg(8, sizeof(int), &argSize);
                        logErr(err, "setArgNRay");
                        cl::Event event;
                        err = m_queue.enqueueNDRangeKernel(m_kernel, cl::NullRange, global, local, NULL, &event);
                        logErr(err, "EnqueueNDKernel");
                        event.wait();
                        err = m_queue.enqueueReadBuffer(m_impacts.m_buffer, CL_TRUE, 0, m_impacts.size(), m_impacts.m_memory.get());
                        logErr(err, "ReadBufferImpact");
                        err = m_queue.enqueueReadBuffer(m_distances.m_buffer, CL_TRUE, 0, m_distances.size(), m_distances.m_memory.get());
                        logErr(err, "ReadBufferDistance");
                        err = m_queue.enqueueReadBuffer(m_normal.m_buffer, CL_TRUE, 0, m_normal.size(), m_normal.m_memory.get());
                        logErr(err, "ReadBufferNormal");

                        impacts.resize(size);
                        for(size_t i = 0; i != size; ++i)
                        {
                            impacts[i].impact = m_impacts.m_memory[i] != 0;
                            assign(impacts[i].normal, m_normal.m_memory[i]);
                            impacts[i].distance = m_distances.m_memory[i];
                        }
                    }

                    ~OpenCLImpl() {--m_nbContexts;}

                    // Non-copyable
                    OpenCLImpl(const OpenCLImpl& other) =delete;
                    OpenCLImpl& operator=(const OpenCLImpl& other) =delete;

                private:
                    cl::Context m_context;
                    std::vector<cl::Device> m_devices;
                    cl::CommandQueue m_queue;
                    cl::Kernel m_kernel;

                    // Buffers
                    OpenCLBuffer<cl_float3> m_raySources;
                    OpenCLBuffer<cl_float3> m_rayDir;
                    OpenCLBuffer<cl_float3> m_trianglesA;
                    OpenCLBuffer<cl_float3> m_trianglesB;
                    OpenCLBuffer<cl_float3> m_trianglesC;
                    OpenCLBuffer<cl_char> m_impacts;
                    OpenCLBuffer<cl_float> m_distances;
                    OpenCLBuffer<cl_float3> m_normal;

                    static uint8_t m_nbContexts;
            };

            uint8_t OpenCLImpl::m_nbContexts = 0;

            // Singleton with the opencl implementation
            OpenCLImpl openCLImpl;
        }

        TriangleIntersector::TriangleIntersector() {} 

        void TriangleIntersector::uploadGeometry(const std::vector<std::shared_ptr<ITriangle> >& triangles)
        {
            openCLImpl.transferTriangles(triangles);
        }

        void TriangleIntersector::computeIntersections(const std::vector<Ray>& rays, std::vector<Impact>& impacts) 
        {
            assert(!rays.empty());
            assert(impacts.empty());

            const size_t size = rays.size();
            openCLImpl.transferRay(rays);
            openCLImpl.createOutputBuffers(size);
            openCLImpl.compute(size, impacts);
        }
    }
}
