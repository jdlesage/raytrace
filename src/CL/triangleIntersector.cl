/**
  Copyright Jean-Denis Lesage (2012)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#pragma OPENCL EXTENSION cl_khr_byte_addressable_store : enable

void intersectTriangle          (const float3 triangleA,
                                const float3 triangleB,
                                const float3 triangleC,
                                const float3 raySource,
                                const float3 rayDir,
                                char* impact,
                                float* dist,
                                float3* normal)
{
    const float3 e1 = triangleB - triangleA; 
    const float3 e2 = triangleC - triangleA;
    const float3 q = cross(rayDir, e2);
    const float a = dot(e1, q);
    if(fabs(a)< 1e-5)
    {
        *impact = 0;
        return;
    }
    const float f = 1.0/a;
    const float3 s = raySource - triangleA;
    const float u = f*dot(s, q);
    if(u < 0.0f)
    {
        *impact = 0;
        return;
    }
    const float3 r = cross(s, e1);
    const float v = f*dot(rayDir, r);
    if(v <0.0f || u+v > 1.0f)
    {
        *impact = 0;
    }
    else
    {
        *dist =  f*dot(e2, r);
        *impact = *dist > 0.0f;
        *normal = normalize(cross(e1, e2));
    }
}


__kernel void intersectRays     (__global const float3* triangleA,
                                __global const float3* triangleB,
                                __global const float3* triangleC,
                                __global const float3* raySource,
                                __global const float3* rayDir,
                                __global char* impact,
                                __global float* dist,
                                __global float3* normal,
                                const int NRay,
                                const int NTriangle)
{
    const int idx = get_global_id(0);
    if(idx < NRay)
    {
        impact[idx] = 0;
        float currDist = 0;
        float3 currNormal;
        char currImpact;
        for(int i = 0; i != NTriangle; ++i)
        {
            intersectTriangle(triangleA[i], triangleB[i], triangleC[i], 
                              raySource[idx], rayDir[idx],
                              &currImpact, &currDist, &currNormal);
            if(currImpact != 0 && (impact[idx] == 0 || currDist < dist[idx]))
            {
                impact[idx] = currImpact;
                dist[idx] = currDist;
                normal[idx] = currNormal;
            }
        }
    }
}
