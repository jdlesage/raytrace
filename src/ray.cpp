/**
  Copyright Jean-Denis Lesage (2012)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include "ray.h"
#include <assert.h>

namespace raytrace
{
    Ray::Ray(const Vector4& source, const Vector4& direction): m_source(source), m_direction(direction)
    {
        assert(equalReal(m_source[3], 1.0));
        assert(equalReal(m_direction[3], 0.0));
        const Real_t n = m_direction.extractVector3().norm();
        m_direction /= n;
    }

    Real_t Ray::getNormDirectionAfterTransformation(const Matrix44& mat) const
    {
        return (mat * m_direction).norm();
    }

    Ray operator*(const Matrix44& mat, const Ray& ray)
    {
        const Vector4 source = mat * ray.m_source;
        Vector4 direction = mat * ray.m_direction;
        Ray result(source, direction);
        return result;
    }
}
