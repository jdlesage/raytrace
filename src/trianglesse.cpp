/**
  Copyright Jean-Denis Lesage (2012)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/
#ifdef __SSE4_1__
#include <assert.h>
#include "trianglesse.h"

namespace raytrace
{
    TriangleSSE::TriangleSSE(const std::vector<Vector3>& vertexBuffer, size_t a, size_t b, size_t c): ITriangle() 
    {
        m_origA = vertexBuffer[a];
        m_origB = vertexBuffer[b];
        m_origC = vertexBuffer[c];
        m_a = m_origA;
        m_b = m_origB;
        m_c = m_origC;
        computeConstants();
    }

    void TriangleSSE::setTransformation(const Matrix44& transformationMatrix)
    {
        Vector4 a({m_origA.x, m_origA.y, m_origA.z, 1.0});
        Vector4 b({m_origB.x, m_origB.y, m_origB.z, 1.0});
        Vector4 c({m_origC.x, m_origC.y, m_origC.z, 1.0});
        m_a = (transformationMatrix * a).extractVector3();
        m_b = (transformationMatrix * b).extractVector3();
        m_c = (transformationMatrix * c).extractVector3();
        computeConstants();
    }

    void TriangleSSE::computeConstants()
    {
        const Vector3 ab = m_b - m_a;
        const Vector3 ac= m_c - m_a;
        m_vecN = cross(ab, ac); 
        float N2 = dot(m_vecN, m_vecN); 
        const Vector3 u = cross(ac, m_vecN) / N2;
        const Vector3 v = cross(m_vecN, ab) / N2;

        float ptU[4];
        float ptV[4];
        float ptN[4];
        ptU[0] = u.x; 
        ptU[1] = u.y;
        ptU[2] = u.z;
        ptU[3] = -dot(u, m_a);
        ptV[0] = v.x;
        ptV[1] = v.y;
        ptV[2] = v.z;
        ptV[3] = -dot(v, m_a);
        ptN[0] = m_vecN.x;
        ptN[1] = m_vecN.y;
        ptN[2] = m_vecN.z;
        ptN[3] = -dot(-m_vecN, m_a);
        m_d = ptN[3];
        m_vecN /= m_vecN.norm();
        m_u = _mm_load_ps(ptU);
        m_v = _mm_load_ps(ptV);
        m_n = _mm_load_ps(ptN);

    }
            
    ITriangle::PlanePosition TriangleSSE::getPositionFollowingPlane(const Vector3& planeNormal, const Vector3& planePosition) const
    {
        uint8_t nbOver = 0;
        uint8_t nbUnder = 0;
        uint8_t nbOnPlane = 0;

        const float fa = dot(m_a - planePosition, planeNormal);
        if(equalReal(fa, 0))
            ++nbOnPlane;
        else
        {
            if(fa > 0)
                ++nbOver;
            else
                ++nbUnder;
        }

        const float fb = dot(m_b - planePosition, planeNormal);
        if(equalReal(fb, 0))
            ++nbOnPlane;
        else
        {
            if(fb > 0)
                ++nbOver;
            else
                ++nbUnder;
        }

        const float fc = dot(m_c - planePosition, planeNormal);
        if(equalReal(fc, 0))
            ++nbOnPlane;
        else
        {
            if(fc > 0)
                ++nbOver;
            else
                ++nbUnder;
        }

        if(nbOnPlane == 3)
            return IS_ON_PLANE;
        if(nbUnder == 0)
            return IS_OVER_PLANE;
        if(nbOver == 0)
            return IS_UNDER_PLANE;
        return INTERSECT_PLANE;
    }


    ITriangle* TriangleSSE::clone() const
    {
        return new TriangleSSE(*this);
    }

    TriangleSSE::TriangleSSE(const TriangleSSE& t) :
        m_u(t.m_u),
        m_v(t.m_v),
        m_n(t.m_n),
        m_vecN(t.m_vecN),
        m_a(t.m_a),
        m_b(t.m_b),
        m_c(t.m_c),
        m_origA(t.m_origA),
        m_origB(t.m_origB),
        m_origC(t.m_origC),
        m_d(t.m_d) {}



    //SSE4 version from Yet Faster Ray-Triangle Intersection (Using SSE4) from
    //J.Havel and A.Herout

    struct Hit
    {
        float px, py, pz, pw;
        float t, u, v;
    };

    const float int_coef_arr[4] = { -1, -1, -1, 1 };
    const __m128 int_coef = _mm_load_ps(int_coef_arr);

    Impact TriangleSSE::intersectInObjectSpace(const Ray& ray) const
    {
        Hit h;
        h.t = 100;
        Impact result;
        const __m128 o = ray.getSourceVector();
        const __m128 d = ray.getDirectionVector();
        const __m128 det = _mm_dp_ps(m_n, d, 0x7f);
        const __m128 dett = _mm_dp_ps(
                _mm_mul_ps(int_coef, m_n), o, 0xff);
        const __m128 oldt = _mm_load_ss(&h.t);
        if((_mm_movemask_ps(_mm_xor_ps(dett,
                            _mm_sub_ss(_mm_mul_ss(oldt, det), dett)))&1) == 0)
        {
            const __m128 detp = _mm_add_ps(_mm_mul_ps(o, det),
                    _mm_mul_ps(dett, d));
            const __m128 detu = _mm_dp_ps(detp, m_u, 0xf1);
            float fdetp[4];
            _mm_store_ps(fdetp, detu);
            if((_mm_movemask_ps(_mm_xor_ps(detu,
                                _mm_sub_ss(det, detu)))&1) == 0)
            {
                const __m128 detv = _mm_dp_ps(detp, m_v, 0xf1);
                const __m128 inv_det = _mm_rcp_ss(det);
                if((_mm_movemask_ps(_mm_xor_ps(detv,
                                    _mm_sub_ss(det, _mm_add_ss(detu, detv))))&1) == 0)
                {
                    _mm_store_ss(&h.t, _mm_mul_ss(dett, inv_det));
                    _mm_store_ss(&h.u, _mm_mul_ss(detu, inv_det));
                    _mm_store_ss(&h.v, _mm_mul_ss(detv, inv_det));
                    _mm_store_ps(&h.px, _mm_mul_ps(detp,
                                _mm_shuffle_ps(inv_det, inv_det, 0)));
                    result.impact = true;
                    result.distance = h.t;
                    result.normal = m_vecN; 
                    return result;
                }
            }
        }
        result.impact = false;
        return result;
    }
}
#endif //__SSE4_1__
