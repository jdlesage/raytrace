/**
  Copyright Jean-Denis Lesage (2012)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include <assert.h>
#include <limits>
#include "bspTree.h"

namespace raytrace
{
    BSPTree::BSPTree(std::shared_ptr<ITriangle>& triangleReference)
    {
        m_planeNormal = triangleReference->getNormal();
        m_planeOrigin = triangleReference->getFirstPoint();
        m_trianglesOnPlane.push_back(triangleReference);
    }
    
    BSPTree* BSPTree::buildBSPTree(std::vector<std::shared_ptr<ITriangle> >& triangles)
    {
        if(triangles.empty())
            return NULL;
        
        // Pop a triangle
        std::shared_ptr<ITriangle> pTriangle(triangles.back());
        assert(pTriangle);
        triangles.pop_back();

        BSPTree* pResult = new BSPTree(pTriangle);

        std::vector<std::shared_ptr<ITriangle> > overTriangles;
        std::vector<std::shared_ptr<ITriangle> > underTriangles;

        for(size_t i = 0; i != triangles.size(); ++i)
        {
            std::shared_ptr<ITriangle> pT(triangles[i]);
            switch(pT->getPositionFollowingPlane(pResult->m_planeNormal, pResult->m_planeOrigin))
            {
                case ITriangle::IS_OVER_PLANE:
                    overTriangles.push_back(pT);
                    break;
                case ITriangle::IS_UNDER_PLANE:
                    underTriangles.push_back(pT);
                    break;
                case ITriangle::IS_ON_PLANE:
                    pResult->m_trianglesOnPlane.push_back(pT);
                    break;
                case ITriangle::INTERSECT_PLANE:
                    {
                    underTriangles.push_back(pT);
                    overTriangles.push_back(pT);
                    }
            }
        }
        // Check recursivity assumption
        assert(!pResult->m_trianglesOnPlane.empty());

        pResult->m_pOverTriangles.reset(BSPTree::buildBSPTree(overTriangles));
        pResult->m_pUnderTriangles.reset(BSPTree::buildBSPTree(underTriangles));

        // Test structure validity
        assert(pResult->isValid());

        return pResult;
    }

    bool BSPTree::isValid() const
    {
        bool bValid = true;
        for(const std::shared_ptr<ITriangle>& pTriangle : m_trianglesOnPlane)
        {
            bValid &= pTriangle->getPositionFollowingPlane(m_planeNormal, m_planeOrigin) == ITriangle::IS_ON_PLANE;    
        }
        if(m_pOverTriangles)
        {
            for(const std::shared_ptr<ITriangle>& pTriangle : m_pOverTriangles->m_trianglesOnPlane)
            {   
                const ITriangle::PlanePosition pos = pTriangle->getPositionFollowingPlane(m_planeNormal, m_planeOrigin);
                bValid &= pos == ITriangle::IS_OVER_PLANE || pos == ITriangle::INTERSECT_PLANE;
            }
            bValid &= m_pOverTriangles->isValid();
        }
        if(m_pUnderTriangles)
        {
            for(const std::shared_ptr<ITriangle>& pTriangle : m_pUnderTriangles->m_trianglesOnPlane)
            {   
                const ITriangle::PlanePosition pos = pTriangle->getPositionFollowingPlane(m_planeNormal, m_planeOrigin);
                bValid &= pos == ITriangle::IS_UNDER_PLANE || pos == ITriangle::INTERSECT_PLANE;
            }
            bValid &= m_pUnderTriangles->isValid();
        }
        return bValid;
    }
            
    Impact BSPTree::intersect(const Ray& ray, const std::pair<Real_t, Real_t>& limits) const 
    {
        Impact i;
        const Real_t det = dot(ray.getSource() - m_planeOrigin, m_planeNormal);
        const Real_t normalDot = dot(ray.getDirection(), m_planeNormal);
        if(equalReal(normalDot, 0))
        {
            // Ray is // to plane
            if(equalReal(det, 0))
            {
                return intersectTriangleAtLevel(ray);
            }
            if(det > 0)
            {
                if(m_pOverTriangles)
                    return m_pOverTriangles->intersect(ray, limits);
                //else
                i.impact = false;
                return i;
            }
            if(det < 0)
            {
                if(m_pUnderTriangles)
                    return m_pUnderTriangles->intersect(ray, limits);
                //else
                i.impact = false;
                return i;

            }
        }
        //else
        // normalDot != 0
        const Real_t k = -det / normalDot;
        if(normalDot > 0)
        {
            // Ray is the same direction to the normal
            if(k >= limits.first && !equalReal(det, 0) && det < 0 && m_pUnderTriangles)
            {
                Impact temp = m_pUnderTriangles->intersect(ray, std::pair<Real_t, Real_t>(limits.first, k));
                if(temp.impact)
                    return temp;
            }
            if(k >= limits.first && k <= limits.second && (det < 0 || equalReal(det, 0)))
            {
                Impact temp = intersectTriangleAtLevel(ray);
                if(temp.impact)
                    return temp;
            }
            if(k <= limits.second && m_pOverTriangles)
            {
                Impact temp = m_pOverTriangles->intersect(ray, std::pair<Real_t, Real_t>(k, limits.second));
                if(temp.impact)
                    return temp;
            }
        }
        else
        {
            // Ray is the opposite direction to the normal
            if(k >= limits.first && !equalReal(det, 0) && det > 0 && m_pOverTriangles)
            {
                Impact temp = m_pOverTriangles->intersect(ray, std::pair<Real_t, Real_t>(limits.first, k));
                if(temp.impact)
                    return temp;
            }
            if(k >= limits.first && k <= limits.second && (det > 0 || equalReal(det, 0)))
            {
                Impact temp = intersectTriangleAtLevel(ray);
                if(temp.impact)
                    return temp;
            }
            if(k <= limits.second && m_pUnderTriangles)
            {
                Impact temp = m_pUnderTriangles->intersect(ray, std::pair<Real_t, Real_t>(k, limits.second));
                if(temp.impact)
                    return temp;
            }

        }
        return i;
    }

    Impact BSPTree::intersectTriangleAtLevel(const Ray& ray) const
    {
        Impact i;
        for(const auto& pTriangle : m_trianglesOnPlane)
        {   
            const Impact imp = std::static_pointer_cast<Object>(pTriangle)->intersect(ray);
            if(imp.impact && (!i.impact || imp.distance < i.distance))
                i = imp;
        }
        return i;
    }

    ViewerBSPTree ViewerBSPTree::getOverBSPTree() const
    {
        assert(HasOverChild());
        ViewerBSPTree res(*(m_tree.m_pOverTriangles));
        return res;
    }

    ViewerBSPTree ViewerBSPTree::getUnderBSPTree() const
    {
        assert(HasUnderChild());
        ViewerBSPTree res(*(m_tree.m_pUnderTriangles));
        return res;
    }

}
