/**
  Copyright Jean-Denis Lesage (2012)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include <algorithm>
#include <boost/cast.hpp>
#include <memory>
#include "mesh.h"
#include "primitiveContainer.h"
#include "triangleFactory.h"

using namespace boost;

namespace raytrace
{
    Mesh::Mesh() : m_vertexBuffer(), m_indexBuffer(),  m_triangleBufferIsValid(true)  
    {
        m_triangles.reset(new PrimitiveContainer); 
    }

    void Mesh::setVertexBuffer(const std::vector<Vector3>& vertexBuffer)
    {
        m_vertexBuffer.resize(vertexBuffer.size());
        std::copy(vertexBuffer.begin(), vertexBuffer.end(), m_vertexBuffer.begin());
        m_triangleBufferIsValid = false;
        generateTriangleBuffer();
    }

    void Mesh::setIndexBuffer(const std::vector<size_t>& indexBuffer)
    {
        m_indexBuffer.resize(indexBuffer.size());
        std::copy(indexBuffer.begin(), indexBuffer.end(), m_indexBuffer.begin());
        m_triangleBufferIsValid = false;
        generateTriangleBuffer();
    }
            
    void Mesh::getTriangles(std::vector<std::shared_ptr<ITriangle> >& triangles)
    {
        assert(triangles.empty());
        assert(m_indexBuffer.size() % 3 == 0);
        for(size_t i = 0; i != m_indexBuffer.size(); i += 3)
        {
            ITriangle *pTriangle = createTriangle(m_vertexBuffer, m_indexBuffer[i], m_indexBuffer[i+1], m_indexBuffer[i+2]); 
            boost::polymorphic_cast<Object*>(pTriangle)->setTransformation(m_transformationMatrix);
            triangles.emplace_back(pTriangle);
        }
    }
            
    Impact Mesh::intersectInObjectSpace(const Ray& ray) const
    {
        assert(m_triangleBufferIsValid);
        std::vector<Ray> rays;
        rays.push_back(ray);
        std::vector<Impact> impacts;
        impacts.resize(1);
        m_triangles->findFirstImpact(rays, impacts);
        return impacts[0];
    }

    void Mesh::generateTriangleBuffer()
    {
        assert(!m_triangleBufferIsValid);
        if(m_indexBuffer.size() % 3 == 0)
        {
            if(!m_indexBuffer.empty() &&  *(std::max_element(m_indexBuffer.begin(), m_indexBuffer.end())) < m_vertexBuffer.size())
            {
                m_triangles->clear();
                // Create triangle
                for(size_t i = 0; i != m_indexBuffer.size() / 3; ++i)
                {
                    assert(m_indexBuffer[3*i] < m_vertexBuffer.size());
                    assert(m_indexBuffer[3*i+1] < m_vertexBuffer.size());
                    assert(m_indexBuffer[3*i+2] < m_vertexBuffer.size());
                    std::shared_ptr<Object> pTriangle(boost::polymorphic_cast<Object*>(createTriangle(m_vertexBuffer, m_indexBuffer[3*i], m_indexBuffer[3*i+1], m_indexBuffer[3*i+2])));
                    pTriangle->setTransformation(m_transformationMatrix);
                    m_triangles->addObject(pTriangle);
                }
                m_triangleBufferIsValid = true;
            }
            if(m_indexBuffer.empty())
                m_triangleBufferIsValid = true;
        }
    }
}
