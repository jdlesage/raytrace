/**
  Copyright Jean-Denis Lesage (2012)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include <assert.h>
#include <math.h>
#include <boost/array.hpp>
#ifdef __SSE4_1__
#include <smmintrin.h>
#endif //__SSE4_1__
#include "types.h"

namespace raytrace
{

    //// VECTOR 3 /////////:
    Vector3::Vector3(const std::initializer_list<float>& list)
    {
        assert(list.size() == 3);
        auto it = list.begin();
        x = *(it++);
        y = *(it++);
        z = *it;
    }

    float Vector3::norm() const
    {
        return sqrtf(x*x +y*y + z*z);
    }
    Vector3 Vector3::operator/(float value) const
    {
        assert(!equalReal(value, 0));
        Vector3 result({x, y, z});
        result.x /= value;
        result.y /= value;
        result.z /= value;
        return result;
    }

    Vector3& Vector3::operator/=(float value)
    {
        assert(!equalReal(value, 0));
        x /= value;
        y /= value;
        z /= value;
        return *this;
    }

    Vector3 Vector3::operator+(const Vector3& other) const
    {
        Vector3 result({x, y, z});
        result.x += other.x;
        result.y += other.y;
        result.z += other.z;
        return result;
    }

    Vector3 Vector3::operator-(const Vector3& other) const
    {
        Vector3 result(x, y, z);
        result.x -= other.x;
        result.y -= other.y;
        result.z -= other.z;
        return result;
    }


    Vector3 Vector3::operator-() const
    {
        return Vector3({-x, -y, -z});
    }

    float& Vector3::operator[] (const size_t value)
    {
        switch(value)
        {
            case 0:
                return x;
            case 1:
                return y;
            case 2:
                return z;
            default:
                assert(false);
                return x;
        }
    }

    float Vector3::operator[] (const size_t value) const
    {
        switch(value)
        {
            case 0:
                return x;
            case 1:
                return y;
            case 2:
                return z;
            default:
                assert(false);
                return 0.0f;
        }
    }

    Vector3 cross(const Vector3& v1, const Vector3& v2)
    {
        return Vector3({v1.y * v2.z - v2.y * v1.z, 
                v1.z * v2.x - v2.z * v1.x,
                v1.x * v2.y - v2.x * v1.y});
    }

    float dot(const Vector3& v1, const Vector3& v2)
    {
    #ifdef __SSE4_1__
        float f[4];
        const __m128 val = _mm_dp_ps(v1.v, v2.v, 0x7F);
        _mm_store_ps(f, val);
        return f[0];
    #else //__SSE4_1__
        return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
    #endif
    }

    Vector3 operator*(const Vector3& vec, float scalar)
    {
        Vector3 result({vec.x, vec.y, vec.z});
        result.x *= scalar;
        result.y *= scalar;
        result.z *= scalar;
        return result;
    }

    bool equal(const Vector3& vec1,  const Vector3& vec2)
    {
        float diff = fabsf(vec1.x - vec2.x) + fabsf(vec1.y - vec2.y) + fabsf(vec1.z - vec2.z);
        return equalReal(diff, 0.0);
    }
    
    std::ostream& operator<<(std::ostream& out, const Vector3& vec)
    {
        out << "[" << vec.x << "\t" << vec.y << "\t" << vec.z << "]";
        return out;
    }

    // VECTOR 4 ////

    Vector4::Vector4() : x(0), y(0), z(0), w(0) {}
    Vector4::Vector4(const std::initializer_list<float>& list)
    {
        assert(list.size() == 4);
        auto it = list.begin();
        x = *(it++);
        y = *(it++);
        z = *(it++);
        w = *it;
    }

    float Vector4::norm() const
    {
        return sqrtf(x*x +y*y + z*z + w*w);
    }
    Vector4 Vector4::operator/(float value) const
    {
        assert(!equalReal(value, 0));
        Vector4 result({x, y, z, w});
        result.x /= value;
        result.y /= value;
        result.z /= value;
        result.w /= value;
        return result;
    }

    Vector4& Vector4::operator/=(float value)
    {
        assert(!equalReal(value, 0));
        x /= value;
        y /= value;
        z /= value;
        w /= value;
        return *this;
    }

    Vector4 Vector4::operator+(const Vector4& other) const
    {
        Vector4 result({x, y, z, w});
        result.x += other.x;
        result.y += other.y;
        result.z += other.z;
        result.w += other.w;
        return result;
    }

    Vector4 Vector4::operator-(const Vector4& other) const
    {
        Vector4 result({x, y, z, w});
        result.x -= other.x;
        result.y -= other.y;
        result.z -= other.z;
        result.w -= other.w;
        return result;

    }

    Vector4 Vector4::operator-() const
    {
        return Vector4({-x, -y, -z, -w});
    }

    float& Vector4::operator[] (const size_t value)
    {
        switch(value)
        {
            case 0:
                return x;
            case 1:
                return y;
            case 2:
                return z;
            case 3:
                return w;
            default:
                assert(false);
                return x;
        }
    }

    float Vector4::operator[] (const size_t value) const
    {
        switch(value)
        {
            case 0:
                return x;
            case 1:
                return y;
            case 2:
                return z;
            case 3:
                return w;
            default:
                assert(false);
                return 0.0f;
        }
    }

    float dot(const Vector4& v1, const Vector4& v2)
    {
    #ifdef __SSE4_1__
        float f[4];
        const __m128 val = _mm_dp_ps(v1.v, v2.v, 255);
        _mm_store_ps(f, val);
        return f[0];
    #else //__SSE4_1__
        return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z + v1.w * v2.w;
    #endif
    }

    Vector4 operator*(const Vector4& vec, float scalar)
    {
        Vector4 result({vec.x, vec.y, vec.z, vec.w});
        result.x *= scalar;
        result.y *= scalar;
        result.z *= scalar;
        result.w *= scalar;
        return result;
    }

    bool equal(const Vector4& vec1,  const Vector4& vec2)
    {
        float diff = fabsf(vec1.x - vec2.x) + fabsf(vec1.y - vec2.y) + fabsf(vec1.z - vec2.z) + fabsf(vec1.w - vec2.w);
        return equalReal(diff, 0.0);
    }

    // Matrix 44 ///////////////:

    Matrix44::Matrix44()
    {
        row[0] = Vector4({1, 0, 0, 0});
        row[1] = Vector4({0, 1, 0, 0});
        row[2] = Vector4({0, 0, 1, 0});
        row[3] = Vector4({0, 0, 0, 1});
    }

    Matrix44::Matrix44(const std::initializer_list<float>& list)
    {
        assert(list.size() == 16);
        size_t i = 0;
        for(auto it : list)
        {
            row[i / 4][i % 4] = it;
            ++i;
        }
    }

    template<size_t N>
        boost::array<Real_t, (N-1)*(N-1)> computeSubMatrix(const boost::array<Real_t, N*N>& mat, size_t row, size_t col)
        {
            boost::array<Real_t, (N-1)*(N-1)> result;
            for(size_t i = 0; i != N-1; ++i)
                for(size_t j = 0; j != N-1; ++j)
                {
                    const size_t i1 = i < row ? i : i+1;
                    const size_t j1 = j < col ? j : j+1;
                    result[j + i*(N-1)] = mat[j1 + i1*N];
                }
            return result;
        }

    template<size_t N>
        Real_t detRec(const boost::array<Real_t, N*N>& mat)
        {
            Real_t result = 0.0f;
            for(size_t i = 0; i != N; ++i)
            {
                boost::array<Real_t, (N-1)*(N-1)> subMatrix = computeSubMatrix<N>(mat, i, 0);
                if(i % 2 == 0)
                {
                    result += mat[i*N] * detRec<N-1>(subMatrix);
                }
                else
                {
                    result -= mat[i*N] * detRec<N-1>(subMatrix);
                }
            }
            return result;
        }

    template<>
        Real_t detRec<2>(const boost::array<Real_t, 4>& mat)
        {
            return mat[0] * mat[3] - mat[1] * mat[2];
        }

    template<>
        Real_t detRec<1>(const boost::array<Real_t, 1>& mat)
        {
            return mat[0];
        }


    Real_t Matrix44::det() const
    {
        boost::array<Real_t, 16> mat;
        for(uint8_t i = 0; i != 4; ++i)
            for(uint8_t j = 0; j != 4; ++j)
                mat[i * 4 + j] = row[i][j];
        return detRec<4>(mat);
    }

    Matrix44 Matrix44::inv() const
    {
        // Compute determinant
        const Real_t determinent = det();
        assert(!equalReal(0, determinent));

        // Copy mat to temp matrix
        boost::array<Real_t, 16> mat;
        for(uint8_t i = 0; i != 4; ++i)
        {
            for(uint8_t j = 0; j != 4; ++j)
            {
                mat[j + i*4] = row[i][j];
            }
        }

        boost::array<Real_t, 16> tempResult;

        // Use Cramer's rule
        for(uint8_t i = 0; i != 4; ++i)
        {
            for(uint8_t j = 0; j != 4; ++j)
            {
                if((i+j) % 2 == 0)
                    tempResult[j + i*4] = detRec<3>(computeSubMatrix<4>(mat, i, j)) / determinent;
                else
                    tempResult[j + i*4] = -detRec<3>(computeSubMatrix<4>(mat, i, j)) / determinent;

            }
        }

        // Copy the transpose
        Matrix44 result;
        for(uint8_t i = 0; i != 4; ++i)
            for(uint8_t j = 0; j != 4; ++j)
                result(i, j) = tempResult[i + 4*j];
        return result;
    }

            
   Matrix44 Matrix44::operator*(const Matrix44& other) const
    {
        Matrix44 result;
        for(size_t i = 0; i != 4; ++i)
        {
            for(size_t j = 0; j != 4; ++j)
            {
                result.row[i][j] = row[i][0] * other.row[0][j] + row[i][1] * other.row[1][j] + row[i][2] * other.row[2][j] + row[i][3] * other.row[3][j];
            }
        } 
        return result;
    }

    Matrix44 Matrix44::operator+(const Matrix44& other) const
    {
        Matrix44 result;
        for(size_t i = 0; i != 4; ++i)
        {
            for(size_t j = 0; j != 4; ++j)
            {
                result.row[i][j] = row[i][j] + other.row[i][j];
            }
        }
        return result;
    }

    Matrix44 Matrix44::operator-(const Matrix44& other) const
    {
        Matrix44 result;
        for(size_t i = 0; i != 4; ++i)
        {
            for(size_t j = 0; j != 4; ++j)
            {
                result.row[i][j] = row[i][j] - other.row[i][j];
            }
        }
        return result;
    }

    float& Matrix44::operator()(size_t i, size_t j)
    {
        assert(i < 4);
        assert(j < 4);
        return row[i][j];
    }

    Vector4 operator*(const Matrix44& mat, const Vector4& vec)
    {
        Vector4 result;
#ifdef __SSE4_1__
        __m128 val = _mm_dp_ps(mat.row[0].v, vec.v, 255);
        _mm_store_ps(&result.x, val);
        val = _mm_dp_ps(mat.row[1].v, vec.v, 255);
        _mm_store_ps(&result.y, val);
        val = _mm_dp_ps(mat.row[2].v, vec.v, 255);
        _mm_store_ps(&result.z, val);
        val = _mm_dp_ps(mat.row[3].v, vec.v, 255);
        _mm_store_ps(&result.w, val);
#else
        result.x = dot(mat.row[0], vec);
        result.y = dot(mat.row[1], vec);
        result.z = dot(mat.row[2], vec);
        result.w = dot(mat.row[3], vec);
#endif
        return result;
    }

    bool equal(const Matrix44& mat1,  const Matrix44& mat2)
    {
        float res = 0;
        for(size_t i = 0; i != 4; ++i)
        {
            res += fabsf(mat1.row[i][0] - mat2.row[i][0]) + fabsf(mat1.row[i][1] - mat2.row[i][1]) + fabsf(mat1.row[i][2] - mat2.row[i][2]) + fabsf(mat1.row[i][3] - mat2.row[i][3]);
        }
        return equalReal(res, 0);
    }

    std::ostream& operator<<(std::ostream& out, const Matrix44& mat) 
    {
        out << "[";
        for(uint8_t i = 0; i != 4; ++i)
        {
            for(uint8_t j = 0; j != 4; ++j)
            {
                out << "\t" << mat.row[i][j];
            }
            out << std::endl;
        }
        out << "]"; 
        return out;
    }

 
    // Matrix33 //////////////

    Matrix33::Matrix33()
    {
        row[0] = Vector3({1, 0, 0});
        row[1] = Vector3({0, 1, 0});
        row[2] = Vector3({0, 0, 1});
    }

    Matrix33::Matrix33(const std::initializer_list<float>& list)
    {
        assert(list.size() == 9);
        size_t i = 0;
        for(auto it : list)
        {
            row[i / 3][i % 3] = it;
            ++i;
        }
    }

    Matrix33 Matrix33::operator+(const Matrix33& other) const
    {
        Matrix33 result;
        for(size_t i = 0; i != 3; ++i)
        {
            for(size_t j = 0; j != 3; ++j)
            {
                result.row[i][j] = row[i][j] + other.row[i][j];
            }
        }
        return result;
    }

    Matrix33 Matrix33::operator-(const Matrix33& other) const
    {
        Matrix33 result;
        for(size_t i = 0; i != 3; ++i)
        {
            for(size_t j = 0; j != 3; ++j)
            {
                result.row[i][j] = row[i][j] - other.row[i][j];
            }
        }
        return result;
    }

    float& Matrix33::operator()(size_t i, size_t j)
    {
        assert(i < 3);
        assert(j < 3);
        return row[i][j];
    }

    Matrix33 operator*(const Matrix33& mat, float scalar)
    {
        Matrix33 result;
        for(size_t i = 0; i != 3; ++i)
        {
            result.row[i] = scalar * mat.row[i];
        }
        return result;
    }

    bool equal(const Matrix33& mat1,  const Matrix33& mat2)
    {
        float res = 0;
        for(size_t i = 0; i != 3; ++i)
        {
            res += fabsf(mat1.row[i][0] - mat2.row[i][0]) + fabsf(mat1.row[i][1] - mat2.row[i][1]) + fabsf(mat1.row[i][2] - mat2.row[i][2]);
        }
        return equalReal(res, 0);
    }


}
