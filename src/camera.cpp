/**
  Copyright Jean-Denis Lesage (2011)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include "camera.h"
#include "matrix.h"
#include <assert.h>
#include <math.h>

namespace raytrace
{

    Camera::Camera() :  m_invProj(),
                        m_invViewMatrix(),
                        m_invViewMatrixProj(),
                        m_position({0, 0, 0}), 
                        m_target({0, 0, 1}), 
                        m_up({0, 1, 0}),
                        m_left(-1),
                        m_right(1),
                        m_bottom(-1),
                        m_top(1),
                        m_near(1),
                        m_far(2),
                        m_width(800),
                        m_height(600)

    {
        m_invProj = perspective(m_left, m_right, m_bottom, m_top, m_near, m_far).inv();
        computeInvViewMatrix();
        m_invViewMatrixProj = m_invViewMatrix * m_invProj; 
    } 

            
    void Camera::setTarget(const Vector3& target)
    {
        assert(target.norm() > 0);
        m_target = target / target.norm();
        computeInvViewMatrix();
        m_invViewMatrixProj = m_invViewMatrix * m_invProj; 
    }
    
    void Camera::setUp(const Vector3& up)
    {
        assert(up.norm() > 0);
        m_up = up /up.norm();
        computeInvViewMatrix();
        m_invViewMatrixProj = m_invViewMatrix * m_invProj; 
    }
            
    void Camera::setPosition(const Vector3& position) 
    {
        m_position = position;
        computeInvViewMatrix();
        m_invViewMatrixProj = m_invViewMatrix * m_invProj; 
    }

    void Camera::computeInvViewMatrix()
    {
        //Matrix camera => world
        assert(equalReal(m_target.norm(), 1));
        assert(equalReal(m_up.norm(), 1));
        const Vector3 x = cross(m_up, m_target);
        Matrix44 temp( {x[0], m_up[0], m_target[0], m_position[0],
                        x[1], m_up[1], m_target[1], m_position[1],
                        x[2], m_up[2], m_target[2], m_position[2],
                        0.0, 0.0, 0.0, 1.0});
        m_invViewMatrix = temp;
    }

    void Camera::setFrustrum(  Real_t left,
            Real_t right,
            Real_t bottom,
            Real_t top,
            Real_t near,
            Real_t far)
    {
        assert(left < right);
        assert(bottom < top);
        assert(near < far);
        assert(near > 0);

        m_left = left;
        m_right = right;
        m_bottom = bottom;
        m_top = top;
        m_near = near;
        m_far = far;
        m_invProj = perspective(m_left, m_right, m_bottom, m_top, m_near, m_far).inv();
        m_invViewMatrixProj = m_invViewMatrix * m_invProj; 
    }

    std::pair<uint32_t, uint32_t> Camera::getViewPort() const
    {
        return std::pair<uint32_t, uint32_t>(m_width, m_height);
    }

    void Camera::setViewPort(uint32_t width, uint32_t height)
    {
        assert(width > 1);
        assert(height > 1);
        m_width = width;
        m_height = height;
    }

    Ray Camera::getRayFromPixel(uint32_t x, uint32_t y) const
    {
        assert(x < m_width);
        assert(y < m_height);

        Vector4 position({m_position[0], m_position[1], m_position[2], 1});
        const Real_t xRatio = 2.0 * (static_cast<Real_t>(x) / static_cast<Real_t>(m_width - 1)) - 1.0;
        const Real_t yRatio = 2.0 * (static_cast<Real_t>(m_height - 1 - y) / static_cast<Real_t>(m_height - 1)) - 1.0;
        const Vector4 screenSpacePixel({xRatio, yRatio, -1.0, 1.0});
        Vector4 worldSpacePixel = m_invViewMatrixProj * screenSpacePixel;
        assert(abs(worldSpacePixel[3]) > 0);
        worldSpacePixel /= worldSpacePixel[3];
        //transform to Vector3
        return Ray(position, worldSpacePixel - position);
    }

}
