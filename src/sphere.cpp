/**
  Copyright Jean-Denis Lesage (2012)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include <algorithm>
#include <assert.h>
#include "sphere.h"

namespace raytrace
{
    Sphere::Sphere() : m_center({0, 0, 0}), m_radiusSquare(1.0), m_radius(1.0) {}

    void Sphere::setRadius(const Real_t& radius)
    {
        m_radiusSquare = radius * radius;
        m_radius = radius;
        assert(equalReal(m_radiusSquare, m_radius * m_radius));
    } 

    Impact Sphere::intersectInObjectSpace(const Ray& ray) const
    {
        Impact result;
        assert(equalReal(ray.getDirection().norm(), 1));
        const Vector3 source = ray.getSource() - m_center;

        // Intersection ray/sphere
        const Real_t b = dot(source, ray.getDirection());
        const Real_t c = dot(source, source) - m_radiusSquare;
        const Real_t delta = b*b - c;

        result.impact = false;
        if(delta >= 0)
        {
            const Real_t l1 = (-b - sqrt(delta));
            const Real_t l2 = (-b + sqrt(delta));
            if(l1 >= 0 && l2 >= 0)
            {
                result.impact = true;
                result.distance = std::min(l1, l2);
            }
            else
            {
                if(l1 >= 0)
                {
                    assert(l2 < 0);
                    result.impact = true;
                    result.distance = l1;
                }
                if(l2 >= 0)
                {
                    assert(l1 < 0);
                    result.impact = true;
                    result.distance = l2;
                }
            }
        }
        // Normal computation
        if(result.impact)
        {
           result.normal = (source + result.distance * ray.getDirection()) / m_radius;
           assert(equalReal(result.normal.norm(), 1));
        }
        return result;
    }
}
