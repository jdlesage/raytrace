/**
  Copyright Jean-Denis Lesage (2012)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include <assert.h>
#include "light.h"

namespace raytrace
{
    Light::Light(const Vector4& position) : m_position(position)
    {
        if(equalReal(position[3], 0.0))
        {
            m_position /= position.norm();
        }
        else
        {
            m_position /= position[3];
        }
    }


    Vector3 Light::getDirectionLight(const Vector3& position) const
    {
        if(equalReal(m_position[3], 0.0))
            return m_position.extractVector3();
        const Vector3 result = position - m_position.extractVector3();
        assert(!equalReal(result.norm(), 0));
        return result / result.norm();
    }
}
