/**
  Copyright Jean-Denis Lesage (2012)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include <assert.h>
#include "ray.h"
#include "triangle.h"

namespace raytrace
{
    Triangle::Triangle(const std::vector<Vector3>& vertexBuffer, size_t a, size_t b, size_t c) : ITriangle()
    {
        m_origA = vertexBuffer[a];
        m_origB = vertexBuffer[b];
        m_origC = vertexBuffer[c];
        m_a = vertexBuffer[a];
        m_b = vertexBuffer[b];
        m_c = vertexBuffer[c];
    }

    Vector3 Triangle::getNormal() const
    {
        Vector3 n= cross(m_b -m_a, m_c - m_a);
        assert(!equalReal(n.norm(), 0));
        n /= n.norm();
        return n;
    }

    void Triangle::setTransformation(const Matrix44& matTransformation)
    { 
        const Vector4 a({m_origA.x, m_origA.y, m_origA.z, 1});
        const Vector4 b({m_origB.x, m_origB.y, m_origB.z, 1});
        const Vector4 c({m_origC.x, m_origC.y, m_origC.z, 1});
        m_a = (matTransformation * a).extractVector3();
        m_b = (matTransformation * b).extractVector3();
        m_c = (matTransformation * c).extractVector3();
    }

    ITriangle::PlanePosition Triangle::getPositionFollowingPlane(const Vector3& planeNormal, const Vector3& planePosition) const
    {
        uint8_t nbOver = 0;
        uint8_t nbUnder = 0;
        uint8_t nbOnPlane = 0;

        const float fa = dot(m_a - planePosition, planeNormal);
        if(equalReal(fa, 0))
            ++nbOnPlane;
        else
        {
            if(fa > 0)
                ++nbOver;
            else
                ++nbUnder;
        }

        const float fb = dot(m_b - planePosition, planeNormal);
        if(equalReal(fb, 0))
            ++nbOnPlane;
        else
        {
            if(fb > 0)
                ++nbOver;
            else
                ++nbUnder;
        }

        const float fc = dot(m_c - planePosition, planeNormal);
        if(equalReal(fc, 0))
            ++nbOnPlane;
        else
        {
            if(fc > 0)
                ++nbOver;
            else
                ++nbUnder;
        }

        if(nbOnPlane == 3)
            return IS_ON_PLANE;
        if(nbUnder == 0)
            return IS_OVER_PLANE;
        if(nbOver == 0)
            return IS_UNDER_PLANE;
        return INTERSECT_PLANE;
    }

    Triangle::Triangle(const Triangle& t) : 
        m_a(t.m_a),
        m_b(t.m_b),
        m_c(t.m_c),
        m_origA(t.m_origA),
        m_origB(t.m_origB),
        m_origC(t.m_origC) {}

    ITriangle* Triangle::clone() const
    {
        return new Triangle(*this);
    }

    Impact Triangle::intersectInObjectSpace(const Ray& ray) const
    {
        // Algo from Real-Time Rendering Thrid edition
        // Author : Akenine-Möller, Haines and Hoffman
        // Chapter 16.8.2
        Impact result;
        const Vector3 e1 = m_b - m_a; 
        const Vector3 e2 = m_c - m_a;
        const Vector3 q = cross(ray.getDirection(), e2);
        const Real_t a = dot(e1, q);
        if(equalReal(a, 0))
        {
            result.impact = false;
            return result;
        }
        const Real_t f = 1.0/a;
        const Vector3 s = ray.getSource() - m_a;
        const Real_t u = f*dot(s, q);
        if(u < 0.0)
        {
            result.impact = false;
            return result;
        }
        const Vector3 r = cross(s, e1);
        const Real_t v = f*dot(ray.getDirection(), r);
        if(v <0.0 || u+v > 1.0)
        {
            result.impact = false;
            return result;
        }
        result.distance = f*dot(e2, r);
        result.impact = result.distance > 0.0f;
        result.normal = cross(e1, e2);
        assert(!equalReal(result.normal.norm(), 0));
        result.normal /= result.normal.norm();
        return result;
    }
}
