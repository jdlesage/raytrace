/**
  Copyright Jean-Denis Lesage (2012)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include <algorithm> 
#include <assert.h> 
#include "primitiveContainer.h"

namespace raytrace
{
    PrimitiveContainer::PrimitiveContainer(): IPrimitiveContainer() {}

    void PrimitiveContainer::addObject(std::shared_ptr<Object>& o)
    {
        m_listPrimitives.push_back(o);
    }


    void PrimitiveContainer::findFirstImpact(const std::vector<Ray>& rays, std::vector<Impact>& impacts) const
    {
        assert(rays.size() == impacts.size());
        for(size_t i = 0; i != rays.size(); ++i)
        {
            const Ray& ray = rays[i];
            Impact& impact = impacts[i];
            std::vector<Impact> rayImpacts;
            std::for_each(m_listPrimitives.begin(), m_listPrimitives.end(),
                    [&ray, &rayImpacts](const std::shared_ptr<Object>& o) {
                    Impact i = o->intersect(ray);
                    if(i.impact)
                    rayImpacts.push_back(i);
                    });
            if(rayImpacts.empty())
            {
                impact.impact = false;
            }
            else
            {
                // Find the min element
                const auto itFirstImpact = min_element(rayImpacts.begin(), rayImpacts.end(), 
                        [](const Impact& impact1, const Impact& impact2) {return impact1.distance <= impact2.distance;});
                impact =  *itFirstImpact;
            }
        }
    }
}
