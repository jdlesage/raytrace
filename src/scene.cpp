/**
  Copyright Jean-Denis Lesage (2012)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include <assert.h>
#include <boost/cast.hpp>
#include <omp.h>
#include "mixPrimitiveContainer.h"
#include "scene.h"
#include "sphere.h"

using namespace std;

namespace raytrace
{

    namespace 
    {
        const uint32_t CHUNK_LINE_SIZE = 16;
    }

    Scene::Scene() : m_camera() 
    {
        m_camera.setPosition(Vector3({0,0, -2}));
        m_camera.setFrustrum(-4.0/3.0, 4.0/3.0 , -1, 1, 1, 2);
        m_pContainer.reset(new MixPrimitiveContainer);
    }

    QImage Scene::render() const
    {
        m_pContainer->lock();
        const std::pair<uint32_t, uint32_t> viewport = m_camera.getViewPort();
        QImage result(viewport.first, viewport.second, QImage::Format_RGB32);
#pragma omp parallel num_threads(omp_get_num_procs() + 1)
        {
            const uint32_t id = omp_get_thread_num();
            const uint32_t NThreads = omp_get_num_threads();
            const uint32_t yStart = id * viewport.second / NThreads;
            const uint32_t yEnd = id == NThreads - 1 ? viewport.second : (id + 1)*viewport.second / NThreads;
            for(uint32_t yChunk = yStart; yChunk < yEnd; yChunk+=CHUNK_LINE_SIZE)
            {
                const uint32_t yChunkEnd = std::min(yEnd, yChunk+CHUNK_LINE_SIZE);
                std::vector<Ray> rays;
                for(uint32_t y = yChunk ; y!= yChunkEnd; ++y)
                {
                    for(uint32_t x = 0; x != viewport.first; ++x)
                    {
                        rays.push_back(m_camera.getRayFromPixel(x,y));
                    }
                }
                std::vector<Vector3> colors;
                colors.resize(rays.size());
                intersectRay(rays, colors);
                size_t it = 0;
                for(uint32_t y = yChunk ; y!= yChunkEnd; ++y)
                {
                    for(uint32_t x = 0; x != viewport.first; ++x)
                    {
                        const Vector3& color = colors[it++];
                        result.setPixel(x, y, qRgb(static_cast<int>(color[0]*255),
                                    static_cast<int>(color[1]*255),
                                    static_cast<int>(color[2]*255)));
                    }
                }
            }
        }
        return result; 
    }

    void Scene::addObject(std::shared_ptr<Object>& o)
    {
        assert(m_pContainer);
        m_pContainer->addObject(o);
    }

    void Scene::intersectRay(const std::vector<Ray>& rays, std::vector<Vector3>& colors) const
    {
        assert(colors.size() == rays.size());
        // TODO lights in scene
        Light defaultLight({1, -1, 1, 0});
        std::vector<Impact> impacts;
        impacts.resize(rays.size());
        m_pContainer->findFirstImpact(rays, impacts);
        for(uint32_t i = 0; i != rays.size(); ++i)
        {
            Vector3& color = colors[i];
            const Ray& ray = rays[i];
            const Impact& firstImpact = impacts[i];
            if(!firstImpact.impact)
                color = Vector3({0, 0, 0});
            else
            {
                //TODO a material
                const Real_t ambiant = 0.2;
                const Real_t diffuse = 0.8;
                color = (ambiant + diffuse * std::max<Real_t>(0.0, dot(-firstImpact.normal, defaultLight.getDirectionLight(ray.getSource() + firstImpact.distance * ray.getDirection())))) * Vector3({1,1,1});
            }
        }
    }
}
