/**
  Copyright Jean-Denis Lesage (2012)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include <assert.h>
#include <omp.h>
#include "mixPrimitiveContainer.h"

namespace raytrace
{

    void MixPrimitiveContainer::addObject(std::shared_ptr<Object>& o)
    {
        m_BSPContainer.addObject(o);
        m_CLContainer.addObject(o);
    }

    size_t MixPrimitiveContainer::size() const
    {
        assert(m_BSPContainer.size() == m_CLContainer.size());
        return m_BSPContainer.size();
    }

    void MixPrimitiveContainer::clear()
    {
        m_BSPContainer.clear();
        m_CLContainer.clear();
    }

    void MixPrimitiveContainer::lock()
    {
        m_BSPContainer.lock();
        m_CLContainer.lock();
    }

    void MixPrimitiveContainer::findFirstImpact(const std::vector<Ray>& rays, std::vector<Impact>& impacts) const
    {
        size_t id = omp_get_thread_num();
        if(id == 0)
            m_CLContainer.findFirstImpact(rays, impacts);
        else
            m_BSPContainer.findFirstImpact(rays, impacts);
    }
}
