/**
  Copyright Jean-Denis Lesage (2012)
  jdlesage@gmail.com

  This software is a computer program whose purpose is an interactive raytracer.

  This software is governed by the CeCILL-C license under French law and
  abiding by the rules of distribution of free software.  You can  use, 
  modify and/ or redistribute the software under the terms of the CeCILL-C
  license as circulated by CEA, CNRS and INRIA at the following URL
  "http://www.cecill.info". 

  As a counterpart to the access to the source code and  rights to copy,
  modify and redistribute granted by the license, users are provided only
  with a limited warranty  and the software's author,  the holder of the
  economic rights,  and the successive licensors  have only  limited
  liability. 

  In this respect, the user's attention is drawn to the risks associated
  with loading,  using,  modifying and/or developing or reproducing the
  software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate,  and  that  also
  therefore means  that it is reserved for developers  and  experienced
  professionals having in-depth computer knowledge. Users are therefore
  encouraged to load and test the software's suitability as regards their
  requirements in conditions enabling the security of their systems and/or 
  data to be ensured and,  more generally, to use and operate it in the 
  same conditions as regards security. 

  The fact that you are presently reading this means that you have had
  knowledge of the CeCILL-C license and that you accept its terms.*/

#include <assert.h>
#include "bspPrimitiveContainer.h"
#include "mesh.h"

namespace raytrace
{
    BSPPrimitiveContainer::BSPPrimitiveContainer(): IPrimitiveContainer(), m_nonTriangleContainer() {}

    void BSPPrimitiveContainer::addObject(std::shared_ptr<Object>& o)
    {
        assert(o.get() != NULL);
        std::shared_ptr<ITriangle> pTriangle = std::dynamic_pointer_cast<ITriangle>(o);
        if(pTriangle.get())
        {
            m_trianglesToInsertInBSP.push_back(pTriangle);
        }
        else
        {
            if(std::dynamic_pointer_cast<Mesh>(o))
            {
                std::vector<std::shared_ptr<ITriangle> > trianglesMesh;
                std::static_pointer_cast<Mesh>(o)->getTriangles(trianglesMesh);
                m_trianglesToInsertInBSP.insert(m_trianglesToInsertInBSP.end(), trianglesMesh.begin(), trianglesMesh.end());
            }
            else
            {
                m_nonTriangleContainer.addObject(o);
            }
        }
        m_objectContainer.push_back(o);
    }

    size_t BSPPrimitiveContainer::size() const
    {
        return m_nonTriangleContainer.size();
    }

    void BSPPrimitiveContainer::clear()
    {
        m_nonTriangleContainer.clear();
        m_pTree.reset();
    }

    void BSPPrimitiveContainer::lock()
    {
        if(!m_pTree && !m_trianglesToInsertInBSP.empty())
        {
            m_pTree.reset(BSPTree::buildBSPTree(m_trianglesToInsertInBSP));
            m_trianglesToInsertInBSP.clear();
        }
        m_nonTriangleContainer.lock();
    }

    void BSPPrimitiveContainer::findFirstImpact(const std::vector<Ray>& rays, std::vector<Impact>& impacts) const
    {
        assert(rays.size() == impacts.size());
        std::vector<Impact> nonTriangleImpact; 
        nonTriangleImpact.resize(rays.size());
        m_nonTriangleContainer.findFirstImpact(rays, nonTriangleImpact);
        for(size_t i = 0; i != rays.size(); ++i)
        {
            const Ray& ray = rays[i];
            const Impact& res = nonTriangleImpact[i];
            Impact& impact = impacts[i];
            if(m_pTree)
            {
                const Impact iTriangle = m_pTree->intersect(ray);
                if(iTriangle.impact && (!res.impact || iTriangle.distance < res.distance))
                    impact = iTriangle;
            }
            else
            {
                impact = res;
            }
        }
    }
}
